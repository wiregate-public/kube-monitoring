# Monitoring System Deployment Documentation

## Overview

This document outlines the deployment strategy for a comprehensive monitoring system leveraging Grafana, Grafana Agent, Loki, Mimir, and the Mimir Alertmanager, all deployed on a MicroK8s cluster.

- [Grafana](../modules/grafana/README.md) serves as the visualization and alert management tool that provides real-time data visualization and deep analytics.
- [Grafana Agent](../modules/grafana-agent/README.md)is utilized for efficient metrics and logs collection. It forwards metrics to Mimir and logs to Loki, optimizing data ingestion and minimizing resource usage on the nodes.
- [Loki](../modules/loki/README.md) is employed for centralized log aggregation, enabling efficient storage, querying, and analysis of log data.
- [Mimir](../modules/mimir/README.md) acts as the scalable, long-term storage solution for metrics, providing robust query performance and reliability. Uses [Mimio](../modules/minio/README.md)
- **Mimir Alertmanager** manages alert routing and notifications but does not generate alerts directly.

## Automated Dashboard Deployment

### Overview

The monitoring system includes an automated mechanism for deploying Grafana dashboards that are specifically designed to visualize metrics stored in Mimir. These dashboards provide valuable insights into the performance and health of the monitored systems.

### Configuration of Dashboards

Dashboards are managed through the `metaMonitoring` section within the Mimir Helm chart. This functionality is designed to streamline the deployment of Grafana dashboards, making them immediately available for use upon deployment.

- **Enabling Dashboards**: Set `dashboards.enabled` to `true` in the `values.yaml` file of the Mimir Helm chart to activate the automated deployment of Grafana dashboards.

```yaml
metaMonitoring:
  dashboards:
    enabled: true
```
#### ConfigMap Settings: 

Dashboards are stored in Kubernetes ConfigMaps, which are annotated and labeled to ensure they are recognized and loaded by Grafana. This is facilitated through Grafana's sidecar, which automatically detects and imports dashboards based on these annotations and labels.

```yaml
metaMonitoring:
  dashboards:
    annotations:
      k8s-sidecar-target-directory: /tmp/dashboards/Mimir Dashboards
    labels:
      grafana_dashboard: "1"
```
#### How It Works
When enabled, the Helm chart automatically creates ConfigMaps containing the dashboard definitions. The specified k8s-sidecar-target-directory annotation informs the Grafana sidecar about where to load the dashboards from within the container's filesystem. The grafana_dashboard label marks the ConfigMap so that the Grafana sidecar can recognize and import the dashboards into Grafana.

#### Benefits
This automated approach eliminates the need for manual dashboard setups, allowing system administrators and users to immediately access pre-configured visualizations of their data. It ensures that the dashboards are correctly configured to display data from Mimir and are consistently updated along with the deployment of the monitoring infrastructure.

The integration of automated dashboard deployment enhances the usability and effectiveness of the monitoring system. It ensures that users have immediate access to essential metrics visualizations, aiding in quick assessment and decision-making based on real-time data from Mimir.

## Alert Management with Prometheus Operator and kube-prometheus-stack

### Integration Overview

While Mimir provides the data storage, the actual alert generation is managed by the Prometheus Operator, utilizing the [kube-prometheus-stack](../modules/kube-prometheus-stack/README.md). This stack is a collection of Kubernetes resources, Grafana dashboards, and Prometheus rules optimized for the Kubernetes ecosystem.

### Configuration of Alerts and Rules

The [kube-prometheus-stack](../modules/kube-prometheus-stack/README.md) includes a wide array of pre-configured alerts and recording rules that monitor various aspects of the Kubernetes environment:

- **defaultRules: create: true** – This option, when set to true, enables the creation of default alert rules in the kube-prometheus-stack context. These rules typically include many predefined alerts that monitor various aspects of the Kubernetes system.

### Mimir Alertmanager Alerts

The notification configuration for these alerts is managed via the `Mimir Alertmanager API`. This setup allows for dynamic adjustment of alert notifications without modifying the core alert logic.

- **Terraform Module** [mimir_alerts](../modules/mimir_alerts/README.md): This module is used to configure the Mimir Alertmanager. It utilizes a template `alert_conf.tpl` to define the notification settings. The necessary variables are set in the `.env` file located at the project root, specifying the contact points for alerts.

### Grafana Alerts

In addition to the automated alerts from Mimir Alertmanager, custom `Grafana alerts` can be defined and managed through a dedicated Terraform module named [grafana_alerts](../modules/grafana_alerts/README.md).

- **Custom Alert Rules**: Custom rules for alerts are defined in the `rules.json` file within the [grafana_alerts](../modules/grafana_alerts/README.md) module. This file contains the JSON configurations for various alert conditions that are specific to the needs of the deployment.

## Summary

This setup effectively utilizes Grafana, Grafana Agent, Loki, Mimir, [prometheus-blackbox-exporter](../modules/prometheus-blackbox-exporter/README.md) and Prometheus Operator within a MicroK8s environment to provide a robust, scalable monitoring solution. The system's architecture ensures comprehensive data collection, storage, and alert management, tailored to the needs of modern infrastructure monitoring.