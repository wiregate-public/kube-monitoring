microk8s_server_name = "mon-test-srv"
cert_manager_email   = "admin@sigmanet.website"
primary_domain       = "sigmanet.website"
namespace            = "monitor"
# SSH key name on Hetzner Project
# ansible_ssh_keys     = ["SergeiSagan"]
ansible_ssh_keys = [
  {
    "key" = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAID1y0LryEBRcddmmZSa/hwleDFBUiYlNNhrHRhp9rb1z sergei.sagan@wiregate.io"
  },
  {
    "key" = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINpxHIjo0Ge9W4VH9Q7Xx0/SVtJcSFPNhFYoa/4WDOh0 Mikhail Shevtsov"
  }
]

gitlab_infra_project    = "sbox1/final-work3-temp"
prod_api_monitor_user   = "dev@test.com"
prod_api_monitor_domain = "test.com"
minio_persistance_size  = "5Gi"

