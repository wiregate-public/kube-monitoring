variable "app_url" {
  description = "Target for blackbox exporter"
  default     = "http://app.example.com"
}

variable "prod_api_monitor_user" {
  description = "Prod API monitor user"
  type        = string
  default     = ""
}

variable "prod_api_monitor_domain" {
  description = "Prod API monitor domain"
  type        = string
  default     = ""
}

variable "hcloud_token" {
  sensitive = true
}

variable "cert_manager_email" {
  description = "Email for certificate notifications"
  type        = string
  default     = "admin@example.com"
}

variable "microk8s_server_name" {
  description = "Microk8s Server Name"
  type        = string
  default     = "microk8s"
}

variable "microk8s_server_type" {
  description = "Microk8s Server Type"
  type        = string
  default     = "cax41"
}

variable "namespace" {
  description = "Default Namespace"
  type        = string
  default     = "default"
}

variable "primary_domain" {
  description = "Primary Domain"
  type        = string
  default     = "example.com"
}

variable "gitlab_base_url" {
  description = "Gitlab Base URL"
  type        = string
  default     = "https://gitlab.com/api/v4/"
}

variable "gitlab_access_token" {
  description = "Gitlab Access Token"
  type        = string
  default     = ""
}

variable "gitlab_infra_project" {
  description = "Gitlab project path to get Secret Variables from"
  type        = string
  default     = "path-to/infra"
}

variable "gitlab_infra_project_id" {
  description = "Gitlab project ID"
  type        = string
  default     = "1"
}

variable "minio_persistance_size" {
  description = "Minio Bucket Persistance Size"
  type        = string
  default     = "10Gi"
}

variable "ansible_ssh_keys" {
  description = "List of SSH Public Keys passed to Ansible Provider"
  type        = list(map(string))
  default     = []
}

variable "grafana_auth" {
  default = "admin:Password"
}

variable "slack_url" {
    default = ""
}

variable "telegram_token" {
    default = ""
}

variable "telegram_chatid" {
    default = ""
}

variable "mimir_metrics_retention_time" {
  default = "30d"
}

variable "loki_logs_retention_time" {
  default    = "360h"
}

locals {
  grafana_hostname = "grafana.${var.primary_domain}"
  mimir_hostname   = "mimir.${var.primary_domain}"
  loki_hostname    = "loki.${var.primary_domain}"
  minio_hostname   = "s3.${var.primary_domain}"
  minio_console_hostname = "s3-console.${var.primary_domain}"

  minio_buckets = [
    "mimir-blocks",
    "mimir-alertmanager",
    "mimir-ruler",
  ]
  blackbox_targets = [
    {
      name = "prod"
      url  = "https://app.${var.primary_domain}"
    }
  ]
  blackbox_additional_modules = {
    prod_auth = {
      http = {
        headers = {
          Content-Type = "application/json"
          Domain       = var.prod_api_monitor_domain
        }
        method                = "POST"
        preferred_ip_protocol = "ip4"
      }
      prober  = "http"
      timeout = "5s"
    }
  }
}
