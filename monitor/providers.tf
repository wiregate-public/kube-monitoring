provider "hcloud" {
  token = var.hcloud_token
}

provider "helm" {
  kubernetes {
    host                   = module.microk8s.kubeconfig["clusters"]["0"]["cluster"]["server"]
    cluster_ca_certificate = base64decode(module.microk8s.kubeconfig["clusters"]["0"]["cluster"]["certificate-authority-data"])
    client_certificate     = base64decode(module.microk8s.kubeconfig["users"]["0"]["user"]["client-certificate-data"])
    client_key             = base64decode(module.microk8s.kubeconfig["users"]["0"]["user"]["client-key-data"])
  }
}

provider "gitlab" {
  base_url = var.gitlab_base_url
  token    = var.gitlab_access_token
}

provider "grafana" {
  url      = "https://${local.grafana_hostname}"
  auth = "admin:${module.grafana.grafana_admin_password}"
}

provider "minio" {
  minio_server   = local.minio_hostname
  minio_region   = "us-east-1"
  minio_user     = module.minio.root_user
  minio_password = module.minio.root_password
  minio_ssl      = true
}