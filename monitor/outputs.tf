output "microk8s_host" {
  value = module.microk8s.host
}

output "microk8s_kubeconfig" {
  value     = yamlencode(module.microk8s.kubeconfig)
  sensitive = true
}

output "grafana_url" {
  value = "https://${local.grafana_hostname}"
}

output "grafana_admin_password" {
  value     = module.grafana.grafana_admin_password
  sensitive = true
}

output "loki_url" {
  value = "https://${local.loki_hostname}"
}

output "loki_credentials" {
  value     = module.loki.credentials
  sensitive = true
}

output "mimir_credentials" {
  value     = module.mimir.credentials
  sensitive = true
}

output "root_password" {
  description = "Minio Root Password"
  value       = module.minio.root_password
  sensitive   = true
}

output "root_user" {
  description = "Minio Root User"
  value       = module.minio.root_user
  sensitive   = false
}