data "hcloud_floating_ip" "grafana_ip" {
  with_selector = "grafana"
}

module "microk8s" {
  source           = "../modules/microk8s-one-ip"
  server_name      = var.microk8s_server_name
  server_type      = var.microk8s_server_type
  ansible_ssh_keys = var.ansible_ssh_keys
  float_ip         = data.hcloud_floating_ip.grafana_ip.id
  float_ip_address = data.hcloud_floating_ip.grafana_ip.ip_address
  providers = {
    hcloud = hcloud
  }
}

module "cert_manager" {
  source                    = "git::https://gitlab.com/wiregate-public/terraform-modules/cert-manager"
  depends_on                 = [module.microk8s]
  cert_manager_email         = var.cert_manager_email
  cert_manager_chart_version = "1.15.0"
  acme_server                = "https://acme-v02.api.letsencrypt.org/directory" 
}

module "ingress" {
  source     = "../modules/ingress"
  depends_on = [module.microk8s]
}

module "minio" {
  source           = "../modules/minio"
  depends_on       = [module.microk8s]
  hostname         = local.minio_hostname
  console_hostname = local.minio_console_hostname
  buckets          = local.minio_buckets
  persistance_size = var.minio_persistance_size
}

module "mimir" {
  source       = "../modules/mimir"
  depends_on   = [module.microk8s, module.minio, module.cert_manager]
  namespace    = var.namespace
  hostname     = local.mimir_hostname
  s3_endpoint  = local.minio_hostname
  s3_accesskey = module.minio.accesskey
  s3_secretkey = module.minio.secretkey
  slack_url    = var.slack_url
  mimir_metrics_retention_time = var.mimir_metrics_retention_time
}

module "loki" {
  source     = "../modules/loki"
  depends_on = [module.microk8s, module.mimir]
  namespace  = var.namespace
  hostname   = local.loki_hostname
  loki_logs_retention_time = var.loki_logs_retention_time  
}

module "grafana_agent" {
  source            = "../modules/grafana-agent"
  depends_on        = [module.microk8s]
  namespace         = var.namespace
  mimir_credentials = module.mimir.credentials
  loki_credentials  = module.loki.credentials
  cluster_name      = var.microk8s_server_name
}

module "grafana" {
  source       = "../modules/grafana"
  depends_on   = [module.microk8s]
  namespace    = var.namespace
  hostname     = local.grafana_hostname
  datasources  = [module.mimir.datasource, module.loki.datasource]
  cluster_name = var.microk8s_server_name
  cluster_ip   = module.microk8s.host
}

# mimir alerts
module "alerts" {
  source        = "../modules/mimir_alerts"
  depends_on    = [module.microk8s, module.grafana, module.mimir]
  namespace     = var.namespace
  mimir_url     = module.mimir.credentials.baseurl
  mimir_name    = module.mimir.credentials.name
  mimir_passw   = module.mimir.credentials.password
  slack_url     = var.slack_url
  grafana_url   = "https://${local.grafana_hostname}"
  grafana_passw = module.grafana.grafana_admin_password
}

module "grafana_alerts" {
  source          = "../modules/grafana_alerts"
  depends_on      = [module.microk8s, module.grafana, module.mimir]
  grafana_auth    = "admin:${module.grafana.grafana_admin_password}"
  def_datasource  = module.mimir.datasource
  slack_url       = var.slack_url
  telegram_token  = var.telegram_token
  telegram_chatid = var.telegram_chatid 
}

module "prometheus_blackbox_exporter" {
  source             = "../modules/prometheus-blackbox-exporter"
  depends_on         = [module.grafana_agent]
  namespace          = var.namespace
  targets            = local.blackbox_targets
  additional_modules = local.blackbox_additional_modules
}

module "grafana_dashboards" {
  source          = "../modules/grafana_dashboards"
  depends_on      = [module.microk8s, module.grafana, module.loki, module.mimir]
}

resource "gitlab_project_variable" "mimir_var" {
  project     = var.gitlab_infra_project_id
  key         = "MIMIR_CREDENTIALS"
  value       = yamlencode(module.mimir.credentials)
  protected   = true
  depends_on  = [module.mimir]
}

resource "gitlab_project_variable" "loki_var" {
  project     = var.gitlab_infra_project_id
  key         = "LOKI_CREDENTIALS"
  value       = yamlencode(module.loki.var_credentials)
  protected   = true
  depends_on  = [module.loki]
}