terraform {
  required_version = ">= 1.0"

  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.44.1"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.12.1"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "16.7.0"
    }

    grafana = {
      source  = "grafana/grafana"
      version = "~> 2.0"
    }

    minio = {
      source  = "aminueza/minio"
      version = ">= 2.0.0"
    }
  }
}
