function cd () {
  if [[ -f .env ]]; then
    echo "Unscoping configuration"
    unset $(cat .env | cut -d'=' -f1)
  fi
  builtin cd "$@"
  if [[ -f .env ]]; then
    set -o allexport
    echo "Sourcing environment from .env"
    source .env
    set +o allexport
  fi
}