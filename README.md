# Infrastructure

Repository contains Infrastructure configuration for [**monitoring system**](monitor/README.md) based on Grafana

## Prerequisites
Copy `.env.example` into the directory `monitor`as `.env`.
```
cp .env.example monitor/.env
```
Update the variables in `.env`.
When changing directories environment variables will be read from `.env` file.

fill in monitor/terraform.tfvar variables

## Starting deploy container
```
docker-compose run --rm app
```
Above command will start interactive shell in
[auto-deploy-image](https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image).
Container contains all the necessary tools such as `kubectl`, `helm`,
`terraform`.

## Terraform
In the `monitor` directory run:
```
ENV=${PWD##*/} envsubst < backend.tf.template > backend.tf
terraform init
```
