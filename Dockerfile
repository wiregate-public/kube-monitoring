FROM registry.gitlab.com/gitlab-org/cluster-integration/auto-deploy-image/v2-66-0:latest AS executor

RUN apk --no-cache add \
      envsubst \
      groff \
      python3 \
      shadow \
      ansible \
      aws-cli \
      bash-completion \
      openssh
COPY dotenv.sh completions.sh /etc/bash/
RUN if [ `uname -m` = 'x86_64' ]; then echo -n 'amd' > /tmp/arch; else echo -n 'arm' > /tmp/arch; fi; \
         ARCH=$(cat /tmp/arch) VERSION='1.5.7' \
      && curl -L -o /tmp/terraform.zip \
         https://releases.hashicorp.com/terraform/${VERSION}/terraform_${VERSION}_linux_${ARCH}64.zip \
      && cd /usr/bin \
      && unzip /tmp/terraform.zip \
      && rm -f /tmp/terraform.zip /tmp/arch
RUN if [ `uname -m` = 'x86_64' ]; then echo -n 'x86_64' > /tmp/arch; else echo -n 'arm' > /tmp/arch; fi; \
         ARCH=$(cat /tmp/arch) VERSION='454.0.0' \
      && curl -L -o /tmp/google-cloud-cli.tar.gz \
         https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-cli-${VERSION}-linux-${ARCH}.tar.gz \
      && cd / \
      && tar xzf /tmp/google-cloud-cli.tar.gz \
      && rm /tmp/google-cloud-cli.tar.gz \
      && /google-cloud-sdk/bin/gcloud components install gke-gcloud-auth-plugin
RUN if [ `uname -m` = 'x86_64' ]; then echo -n 'amd' > /tmp/arch; else echo -n 'arm' > /tmp/arch; fi; \
         ARCH=$(cat /tmp/arch) VERSION='1.40.0' \
      && curl -L -o /tmp/hcloud.tar.gz \
         https://github.com/hetznercloud/cli/releases/download/v${VERSION}/hcloud-linux-${ARCH}64.tar.gz \
      && cd /usr/sbin \
      && tar xzf /tmp/hcloud.tar.gz hcloud \
      && rm -f /tmp/hcloud.tar.gz /tmp/arch
RUN ln -s /app/.bash_history /root/.bash_history

ENV PATH /google-cloud-sdk/bin:${PATH}

ENTRYPOINT ["sh", "-c", \
     "useradd -u $(stat -c '%u' /app) admin || bash; \
      ln -s /app/.bash_history /home/admin/.bash_history; \
      chown admin:admin /home/admin; \
      su admin -c 'bash'"]
