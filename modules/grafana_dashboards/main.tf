resource "grafana_dashboard" "loki_log" {
  config_json = file("${path.module}/loki_logs.json")
  folder      = 0
}

resource "grafana_dashboard" "loki_log_with_rates" {
  config_json = file("${path.module}/loki_logs_with_rates.json")
  folder      = 0
}