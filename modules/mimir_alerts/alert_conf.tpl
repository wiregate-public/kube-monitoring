template_files:
    default_template: |
        {{ define "__alertmanager" }}AlertManager{{ end }}
        {{ define "__alertmanagerURL" }}{{ .ExternalURL }}/#/alerts?receiver={{ .Receiver | urlquery }}{{ end }}
alertmanager_config: |
    global:
      resolve_timeout: 5m
    templates:
      - 'default_template'
    route:
      group_by: ['alertname', 'job']
      group_wait: 30s
      group_interval: 5m
      repeat_interval: 24h
      receiver: 'slack-notifications'
      routes:
        - receiver: 'null'
          matchers:
            - alertname=~"InfoInhibitor|Watchdog|KubeControllerManagerDown"
        - receiver: 'null'
          matchers:
            - severity=info            
        - receiver: 'slack-notifications'
          matchers:
            - severity=critical          
    receivers:
      - name: 'slack-notifications'
        slack_configs:
          - send_resolved: true
            text: "{{ range .Alerts }}{{ .Annotations.description }}\n{{ end }}"
            api_url: '${slack_url}' 
            channel: '#alerts'
      - name: 'null'
