locals {
  alertmanager_config = templatefile("${path.module}/alert_conf.tpl", {
    slack_url = var.slack_url
    email_to   = var.email_to
  })
}

resource "local_file" "alertmanager_config" {
  content  = local.alertmanager_config
  filename = "${path.module}/alertmanager_config.yaml"
}

resource "null_resource" "update_alertmanager_config" {

  triggers = {
    alertmanager_config_content = local.alertmanager_config 
  }

  provisioner "local-exec" {
    command = <<-EOT
    curl -u ${var.mimir_name}:${var.mimir_passw} -X POST \
      -H "X-Scope-OrgID":'' \
      --data-binary "@${local_file.alertmanager_config.filename}" \
      ${var.mimir_url}/api/v1/alerts
    EOT
  }
}
