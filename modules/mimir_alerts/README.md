# mimir_alerts Module

The `mimir_alerts` module is designed to configure notification rules for alerts that Mimir has automatically collected from the Prometheus Operator's `kube-prometheus-stack`. The purpose of this module is to push alert notification rules into Mimir's API since they are not configured by default.

## Overview

### Alert Management with Prometheus Operator and kube-prometheus-stack

#### Integration Overview
While Mimir provides the data storage, the actual alert generation is managed by the Prometheus Operator, utilizing the `kube-prometheus-stack`. This stack includes a collection of Kubernetes resources, Grafana dashboards, and Prometheus rules optimized for monitoring Kubernetes environments.

#### Configuration of Alerts and Rules
The `kube-prometheus-stack` includes a wide array of pre-configured alerts and recording rules that monitor various aspects of the Kubernetes environment:
- **defaultRules: create: true** – Enables the creation of default alert rules in the `kube-prometheus-stack` context, which monitor various aspects of the Kubernetes system.

#### Mimir Alertmanager Alerts
The notification configuration for these alerts is managed via the **Mimir Alertmanager API**. This setup allows for dynamic adjustment of alert notifications without modifying the core alert logic using the `mimir_alerts` Terraform module, which utilizes a template `alert_conf.tpl` to define the notification settings.

## Resources Created by the Module

The module creates the following resources:

- **locals "alertmanager_config"**: Generates the alert manager configuration file using template variables.

- **resource "local_file" "alertmanager_config"**: Creates a local file with the alert manager configuration.
  ```hcl
  resource "local_file" "alertmanager_config" {
    content  = local.alertmanager_config
    filename = "${path.module}/alertmanager_config.yaml"
  }
  ```

- **resource "null_resource" "update_alertmanager_config"**: Pushes the alert manager configuration to Mimir using a local exec provisioner.

## Variables

The module accepts the following parameters:

- **variable "namespace"**: The namespace in which to install the module.
  ```hcl
  variable "namespace" {
    description = "Namespace to Install"
    default     = ""
  }
  ```

- **variable "mimir_url"**: The URL for the Mimir instance.
  ```hcl
  variable "mimir_url" {
    description = "Mimir URL"
    default     = "mimir.example.com"
  }
  ```

- **variable "mimir_name"**: The username for authenticating with Mimir.
  ```hcl
  variable "mimir_name" {
    description = "Mimir Username"
    default     = "mimir"
  }
  ```

- **variable "mimir_passw"**: The password for authenticating with Mimir.
  ```hcl
  variable "mimir_passw" {
    description = "Mimir Password"
    default     = "Password"
  }
  ```

- **variable "grafana_url"**: The URL for the Grafana instance.
  ```hcl
  variable "grafana_url" {
    description = "Grafana URL"
    default     = "grafana.example.com"
  }
  ```

- **variable "grafana_passw"**: The password for authenticating with Grafana.
  ```hcl
  variable "grafana_passw" {
    description = "Grafana Password"
    default     = "Password"
  }
  ```

- **variable "slack_url"**: The Slack Webhook URL for sending alerts.
  ```hcl
  variable "slack_url" {
    description = "Slack Webhook URL"
    default     = ""
  }
  ```

- **variable "email_to"**: The email address to send alerts to.
  ```hcl
  variable "email_to" {
    description = "Email Address for Alerts"
    default     = "test@test.com"
  }
  ```

### Example Usage

Here’s an example of how to use the `mimir_alerts` module in your Terraform script:

```hcl
module "mimir_alerts" {
  source        = "../modules/mimir_alerts"
  namespace     = "monitor"
  mimir_url     = module.mimir.credentials.baseurl
  mimir_name    = module.mimir.credentials.name
  mimir_passw   = module.mimir.credentials.password
  slack_url     = var.slack_url
  grafana_url   = "https://${local.grafana_hostname}"
  grafana_passw = module.grafana.grafana_admin_password
}
```

## Summary

This `mimir_alerts` module is designed to push alert notification rules into the Mimir Alertmanager API. It leverages pre-configured alerts and recording rules from the Prometheus Operator's `kube-prometheus-stack`, integrating with various contact points such as Slack and email. By configuring these notifications dynamically, the module ensures a robust and flexible alert management system tailored to the needs of modern infrastructure monitoring.
