  variable "namespace" {
    default = ""
  } 

  variable "mimir_url" {
    default = "mimir.example.com"
  }

  variable "mimir_name" {
    default = "mimir"
  }

  variable "mimir_passw" {
    default = "Password"
  }

  variable "grafana_url" {
    default = "grafana.example.com"
  }

  variable "grafana_passw" {
    default = "Password"
  }

  variable "slack_url" {
    default = ""
  }

  variable "email_to" {
    default = "test@test.com"
  }