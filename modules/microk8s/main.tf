resource "tls_private_key" "terraform_ssh_key" {
  algorithm = "ED25519"
}

resource "local_sensitive_file" "terraform_ssh_key" {
  content  = resource.tls_private_key.terraform_ssh_key.private_key_openssh
  filename = "${path.module}/${var.server_name}-terraform.pem"
}

resource "hcloud_ssh_key" "default" {
  name       = "Terraform Managed ${var.server_name}"
  public_key = resource.tls_private_key.terraform_ssh_key.public_key_openssh
}

resource "hcloud_server" "server" {
  name        = var.server_name
  image       = var.server_image
  server_type = var.server_type
  datacenter  = var.datacenter
  # # for this to work this should be your default ssh key
  # ssh_keys    = var.ansible_ssh_keys
  ssh_keys    = [resource.hcloud_ssh_key.default.id]
  lifecycle {
    ignore_changes = [
      ssh_keys,
    ]
  }
  public_net {
    ipv4_enabled = true
    ipv6_enabled = false
  }
}


resource "time_sleep" "wait_30_seconds" {
  depends_on = [resource.hcloud_server.server]
  create_duration = "30s"
}


resource "ansible_playbook" "microk8s" {
  replayable = true
  name       = resource.hcloud_server.server.name
  playbook   = "${path.module}/ansible/playbook.yml"
  groups     = ["all", "ssh", "firewall", "microk8s"]
  extra_vars = {
    ansible_host                 = resource.hcloud_server.server.ipv4_address,
    ansible_user                 = "root",
    ansible_ssh_private_key_file = "${path.module}/${var.server_name}-terraform.pem",
    ssh_keys                     = jsonencode(var.ansible_ssh_keys),
  }
  ignore_playbook_failure = false
  limit                   = [var.server_name]
  depends_on              = [resource.time_sleep.wait_30_seconds]
}


data "external" "kubeconfig" {
  program = [
    "ssh",
    "-o",
    "StrictHostKeyChecking=no",
    "-o",
    "ConnectTimeout=60",
    "-i",
    "${path.module}/${var.server_name}-terraform.pem",
    "root@${resource.hcloud_server.server.ipv4_address}",
    "echo { \\\"output\\\" : \\\"$(microk8s config | base64 -w 0)\\\" }"
  ]
  depends_on = [
    resource.ansible_playbook.microk8s
  ]
}


resource "null_resource" "cleanup" {
  depends_on = [
    data.external.kubeconfig,
    resource.ansible_playbook.microk8s
  ]
  triggers = {
    null_id = resource.local_sensitive_file.terraform_ssh_key.id
  }
  provisioner "local-exec" {
    command = "rm -f ${path.module}/${var.server_name}-terraform.pem"
  }
}


resource "terraform_data" "kubeconfig" {
  input = data.external.kubeconfig.result["output"]
  lifecycle {
    ignore_changes       = [input]
    replace_triggered_by = [resource.ansible_playbook.microk8s]
  }
}
