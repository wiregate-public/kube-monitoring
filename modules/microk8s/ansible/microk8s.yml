---
- hosts: microk8s
  tasks:
    - name: Increase fs.inotify.max_user_watches
      sysctl:
        name: fs.inotify.max_user_watches
        value: '1048576'
        sysctl_set: yes
        state: present
        reload: yes

    - name: Enable vm.nr_hugepages
      sysctl:
        name: vm.nr_hugepages
        value: '1024'
        sysctl_set: yes
        state: present
        reload: yes

    - name: Add nvme-tcp into microk8s-mayastor.conf
      lineinfile:
        path: /etc/modules-load.d/microk8s-mayastor.conf
        line: 'nvme-tcp'
        create: yes

    - name: Load nvme-tcp module
      command: modprobe nvme-tcp

    - name: Update apt cache if older than 7 days
      apt:
        update_cache: "yes"
        cache_valid_time: 604800

    - name: Install snapd if not present
      apt:
        name: "snapd"
        state: "present"

    - name: Disable snap auto-updates unconditionally
      command: snap refresh --hold=forever

    - name: Install or remove microk8s snap packages
      snap:
        name: "{{ item.name }}"
        state: "{{ item.state | default('present') }}"
        classic: "{{ item.classic | default(omit) }}"
        channel: "{{ item.channel | default(omit) }}"
      with_items: "{{ microk8s_snap_packages | default([]) }}"

    - name: Add IP address to /var/snap/microk8s/current/certs/csr.conf.template file
      lineinfile:
        dest: /var/snap/microk8s/current/certs/csr.conf.template
        regexp: 'IP.99 = .*$'
        insertafter: "^#MOREIPS$"
        line: "IP.99 = {{ ansible_host }}"
        state: present
      register: csr_template

    - name: Regenerate microk8s certificates
      command: microk8s refresh-certs --cert ca.crt
      when: csr_template.changed

    - name: Enable addons if any
      shell: "microk8s enable {{ item }}"
      with_items: "{{ microk8s_addons | default([]) }}"
      when: csr_template.changed
