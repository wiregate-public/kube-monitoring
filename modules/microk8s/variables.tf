variable "server_name" {
  description = "Server Name"
  type        = string
  default     = "server1"
}

variable "server_type" {
  description = "Server Type"
  type        = string
  default     = "cx11"
}

variable "server_image" {
  description = "Server Image"
  type        = string
  default     = "ubuntu-22.04"
}

variable "datacenter" {
  description = "Server Datacenter"
  type        = string
  default     = "nbg1-dc3"
}

variable "ansible_ssh_keys" {
  description = "List of SSH Public Keys passed to Ansible Provider"
  type        = list(map(string))
  default     = []
}
