variable "chart_version" {
  description = "Chart Version"
  type        = string
  default     = "7.3.12" #"7.2.4"
}

variable "namespace" {
  description = "Namespace to Install"
  type        = string
  default     = "monitor"
}

variable "hostname" {
  description = "Hostname For Ingress"
  type        = string
  default     = "grafana.local"
}

variable "datasources" {
  description = "Datasources"
  type        = any
  default     = []
}

variable "cluster_name" {
  description = "Cluster Name"
  type        = string
  default     = "kubernetes"
}

variable "cluster_ip" {
  description = "Cluster IP"
  type        = string
  default     = "127.0.0.1"
}

locals {
  chart_values                       = <<-VALUES
    adminUser: admin
    adminPassword: "${resource.random_password.grafana_admin.result}"
    persistence:
      enabled: false
    ingress:
      enabled: true
      ingressClassName: nginx
      annotations:
        ingress.kubernetes.io/ssl-redirect: "true"
        kubernetes.io/tls-acme: "true"
      hosts:
      - "${var.hostname}"
      tls:
      - secretName: "${replace(var.hostname, ".", "-")}-tls"
        hosts:
        - "${var.hostname}"
    datasources:
      datasources.yaml:
        apiVersion: 1
        datasources:
          ${indent(6, yamlencode(var.datasources))}
    dashboards:
      default:
        blackbox:
          gnetId: 14928
          revision: 6
          datasource: "Mimir"
    dashboardProviders:
      dashboardproviders.yaml:
        apiVersion: 1
        providers:
        - name: "default"
          orgId: 1
          folder: ""
          type: "file"
          disableDeletion: false
          editable: true
          options:
            path: "/var/lib/grafana/dashboards"
            foldersFromFilesStructure: false
    sidecar:
      dashboards:
        enabled: true
        label: grafana_dashboard
        labelValue: "1"
        searchNamespace: ALL
        provider:
          allowUiUpdates: false
    serviceMonitor:
      enabled: true
    VALUES
  kube_prometheus_stack_chart_values = <<-VALUES
    crds:
      enabled: true
    coreDns:
      serviceMonitor:
        relabelings:
        - action: replace
          replacement: ${var.cluster_name}
          targetLabel: cluster
    defaultRules:
      create: true
    alertmanager:
      enabled: false
    grafana:
      enabled: false
      forceDeployDashboards: true
      sidecar:
        dashboards:
          multicluster:
            global:
              enabled: true
            etcd:
              enabled: false
    prometheus:
      enabled: false
    prometheusOperator:
      enabled: false
    thanosRuler:
      enabled: false
    kubeApiServer:
      serviceMonitor:
        metricRelabelings:
          - action: drop
            regex: apiserver_request_duration_seconds_bucket;(0.15|0.2|0.3|0.35|0.4|0.45|0.6|0.7|0.8|0.9|1.25|1.5|1.75|2|3|3.5|4|4.5|6|7|8|9|15|25|40|50)
            sourceLabels:
              - __name__
              - le
          - action: drop
            regex: "kubeproxy_.*"
            sourceLabels:
              - __name__
        relabelings:
          - action: replace
            replacement: ${var.cluster_name}
            targetLabel: cluster
    kubeProxy:
      endpoints:
      - ${var.cluster_ip}
      service:
        enabled: true
        port: 10250
        targetPort: 10250
      serviceMonitor:
        enabled: true
        https: true
        interval: 15s
        metricRelabelings:
        - action: keep
          sourceLabels: [__name__]
          regex: "kubeproxy_.*|rest_client_.*"
        relabelings:
          - action: replace
            replacement: ${var.cluster_name}
            targetLabel: cluster
    kubeScheduler:
      endpoints:
      - ${var.cluster_ip}
      service:
        enabled: true
        port: 10250
        targetPort: 10250
      serviceMonitor:
        enabled: true
        https: true
        interval: 15s
        metricRelabelings:
        - action: keep
          sourceLabels: [__name__]
          regex: "scheduler_.*"
        relabelings:
          - action: replace
            replacement: ${var.cluster_name}
            targetLabel: cluster
    kubeStateMetrics:
      enabled: true
    kube-state-metrics:
      prometheus:
        monitor:
          enabled: true
          relabelings:
          - action: replace
            replacement: ${var.cluster_name}
            targetLabel: cluster
    nodeExporter:
      enabled: true
      operatingSystems:
        darwin:
          enabled: false
        linux:
          enabled: true
    prometheus-node-exporter:
      prometheus:
        monitor:
          enabled: true
          relabelings:
          - action: replace
            replacement: ${var.cluster_name}
            targetLabel: cluster
          interval: 30s
    VALUES
}
