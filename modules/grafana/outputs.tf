output "grafana_admin_password" {
  value     = resource.random_password.grafana_admin.result
  sensitive = true
}
