# grafana Module

The `grafana` module deploys Grafana and a Prometheus monitoring stack using Helm charts. It includes a Grafana instance with predefined dashboards, datasources, and other configurations.

## Resources Created by the Module

- **resource "random_password" "grafana_admin"**: Generates a random password for the Grafana admin user.

- **resource "helm_release" "kube_prometheus_stack"**: Installs the `kube-prometheus-stack` Helm chart.

- **resource "helm_release" "grafana"**: Installs the `grafana` Helm chart.


## Variables

The module accepts the following parameters:

- **variable "chart_version"**: The version of the Grafana Helm chart to deploy.
  ```hcl
  variable "chart_version" {
    description = "Chart Version"
    type        = string
    default     = "7.2.4"
  }
  ```

- **variable "namespace"**: The namespace in which to install the Helm releases.
  ```hcl
  variable "namespace" {
    description = "Namespace to Install"
    type        = string
    default     = "monitor"
  }
  ```

- **variable "hostname"**: The hostname for ingress.
  ```hcl
  variable "hostname" {
    description = "Hostname For Ingress"
    type        = string
    default     = "grafana.local"
  }
  ```

- **variable "datasources"**: A list of datasources for Grafana.
  ```hcl
  variable "datasources" {
    description = "Datasources"
    type        = any
    default     = []
  }
  ```

- **variable "cluster_name"**: The name of the Kubernetes cluster.
  ```hcl
  variable "cluster_name" {
    description = "Cluster Name"
    type        = string
    default     = "kubernetes"
  }
  ```

- **variable "cluster_ip"**: The IP address of the Kubernetes cluster.
  ```hcl
  variable "cluster_ip" {
    description = "Cluster IP"
    type        = string
    default     = "127.0.0.1"
  }
  ```

## Local Values

Local values used for Helm chart configuration:

- **chart_values**: Configuration values for the Grafana Helm chart.

- **kube_prometheus_stack_chart_values**: Configuration values for the kube-prometheus-stack Helm chart.


### Example Usage

Here’s an example of how to use the `grafana` module in your Terraform script:

```hcl
module "grafana" {
  source = "../modules/grafana"

  chart_version = "7.2.4"
  namespace     = "monitor"
  hostname      = "grafana.local"
  datasources   = [
    {
      name      = "Prometheus"
      type      = "prometheus"
      url       = "http://prometheus.monitor.svc.cluster.local:9090"
      access    = "proxy"
      isDefault = true
    }
  ]
  cluster_name  = "kubernetes"
  cluster_ip    = "127.0.0.1"
}
```