variable "chart_version" {
  description = "Chart Version"
  type        = string
  default     = "59.1.0"
}

variable "namespace" {
  description = "Namespace to Install"
  type        = string
  default     = "monitor"
}

variable "cluster_name" {
  description = "Cluster Name"
  type        = string
  default     = "kubernetes"
}

locals {
  chart_values = <<-VALUES
    crds:
      enabled: true
    coreDns:
      serviceMonitor:
        relabelings:
        - action: replace
          replacement: ${var.cluster_name}
          targetLabel: cluster
    defaultRules:
      create: true
    alertmanager:
      enabled: false
    grafana:
      enabled: false
      forceDeployDashboards: false
    prometheus:
      enabled: false
    prometheusOperator:
      enabled: false
    thanosRuler:
      enabled: false
    kubeApiServer:
      serviceMonitor:
        metricRelabelings:
          - action: drop
            regex: apiserver_request_duration_seconds_bucket;(0.15|0.2|0.3|0.35|0.4|0.45|0.6|0.7|0.8|0.9|1.25|1.5|1.75|2|3|3.5|4|4.5|6|7|8|9|15|25|40|50)
            sourceLabels:
              - __name__
              - le
          - action: drop
            regex: "kubeproxy_.*"
            sourceLabels:
              - __name__
        relabelings:
          - action: replace
            replacement: ${var.cluster_name}
            targetLabel: cluster
    kubeProxy:
      service:
        enabled: true
      serviceMonitor:
        enabled: true
        https: true
        interval: 15s
        metricRelabelings:
        - action: keep
          sourceLabels: [__name__]
          regex: "kubeproxy_.*|rest_client_.*"
        relabelings:
          - action: replace
            replacement: ${var.cluster_name}
            targetLabel: cluster
    kubeScheduler:
      service:
        enabled: true
      serviceMonitor:
        enabled: true
        https: true
        interval: 15s
        metricRelabelings:
        - action: keep
          sourceLabels: [__name__]
          regex: "scheduler_.*"
        relabelings:
          - action: replace
            replacement: ${var.cluster_name}
            targetLabel: cluster
    kubeStateMetrics:
      enabled: true   
    kube-state-metrics:
      prometheus:
        monitor:
          enabled: true
          relabelings:
          - action: replace
            replacement: ${var.cluster_name}
            targetLabel: cluster
      resources:
        requests:
          memory: "64Mi"
          cpu: "40m"
        limits:
          memory: "512Mi"
          cpu: "500m" 
    nodeExporter:
      enabled: true
      operatingSystems:
        darwin:
          enabled: false
        linux:
          enabled: true          
    prometheus-node-exporter:
      prometheus:
        monitor:
          enabled: true
          relabelings:
          - action: replace
            replacement: ${var.cluster_name}
            targetLabel: cluster
          interval: 30s
      resources:
        requests:
          memory: "64Mi"
          cpu: "40m"
        limits:
          memory: "512Mi"
          cpu: "500m"          
    VALUES
}
