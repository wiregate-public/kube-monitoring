# kube-prometheus-stack Module

The `kube-prometheus-stack` module deploys a comprehensive monitoring solution using the `kube-prometheus-stack` Helm chart from the repository [https://prometheus-community.github.io/helm-charts](https://prometheus-community.github.io/helm-charts). This stack integrates Prometheus, Alertmanager, and various exporters to provide robust monitoring for Kubernetes clusters.

## Resources Created by the Module

- **resource "helm_release" "kube_prometheus_stack"**: Installs the `kube-prometheus-stack` Helm chart.


## Variables

The module accepts the following parameters:

- **variable "chart_version"**: The version of the `kube-prometheus-stack` Helm chart to deploy.
  ```hcl
  variable "chart_version" {
    description = "Chart Version"
    type        = string
    default     = "55.7.1"
  }
  ```

- **variable "namespace"**: The namespace in which to install the Helm release.
  ```hcl
  variable "namespace" {
    description = "Namespace to Install"
    type        = string
    default     = "monitor"
  }
  ```

- **variable "cluster_name"**: The name of the Kubernetes cluster.
  ```hcl
  variable "cluster_name" {
    description = "Cluster Name"
    type        = string
    default     = "kubernetes"
  }
  ```

## Local Values

Local values used for Helm chart configuration:

- **chart_values**: Configuration values for the `kube-prometheus-stack` Helm chart.
  ```hcl
  locals {
    chart_values = <<-VALUES
    ...
    VALUES
  }
  ```

### Example Usage

Here’s an example of how to use the `kube-prometheus-stack` module in your Terraform script:

```hcl
module "kube_prometheus_stack" {
  source = "../modules/kube-prometheus-stack"

  chart_version = "55.7.1"
  namespace     = "monitor"
  cluster_name  = "kubernetes"
}
```

## Functions

The `kube-prometheus-stack` serves as a comprehensive monitoring solution for Kubernetes clusters. Key functions include:

- **Prometheus**: Collects and stores metrics from various sources. It enables powerful queries and visualizations of time-series data.
- **Alertmanager**: Manages alerts and notifications based on Prometheus metrics. It can send alerts to various messaging platforms.
- **Grafana**: Provides monitoring dashboards for visualizing metrics data. It allows integration with multiple data sources.
- **Node Exporter**: Collects hardware and OS metrics of the Kubernetes nodes.
- **Kube State Metrics**: Listens to the Kubernetes API server and generates metrics about the state of objects.
- **Service Monitors**: Allow Prometheus to monitor the services in the cluster. This includes monitoring core DNS, API server, and other essential components.
- **Custom Rules and Alerts**: Define custom rules and alerts for your Kubernetes cluster, helping to stay on top of potential issues.
- **Thanos Ruler**: Allows the evaluation of Prometheus rules and alerts persistently, in clusters where Thanos is used to aggregate Prometheus instances.
