# mimir Module

The `mimir` module deploys Grafana Mimir to Azure Kubernetes Service (AKS) cluster, an open-source distributed and horizontally scalable time series database, using Helm chart from the repository [https://grafana.github.io/helm-charts](https://grafana.github.io/helm-charts). Mimir provides long-term storage for Prometheus metrics and integrates seamlessly with Grafana.

## Overview

Grafana Mimir supports high availability and resilient architectures and provides advanced querying capabilities for large-scale monitoring systems. It also supports alerting and is compatible with Prometheus' remote write API, making it a robust choice for time-series data storage and management.

## Resources Created by the Module

- **module "dummy_helm_chart"**: A dummy Helm chart module used as a placeholder for additional resources.

- **resource "random_password" "ingress_password"**: Generates a random password for the ingress user.

- **resource "helm_release" "mimir"**: Installs the Mimir Helm chart.

- **resource "helm_release" "mimir_resources"**: Installs additional Mimir resources using a dummy Helm chart.


## Variables

The module accepts the following parameters:

- **variable "chart_version"**: The version of the Mimir Helm chart to deploy.
  ```hcl
  variable "chart_version" {
    description = "Chart Version"
    type        = string
    default     = "5.2.1"
  }
  ```

- **variable "namespace"**: The namespace in which to install the Helm releases.
  ```hcl
  variable "namespace" {
    description = "Namespace to Install"
    type        = string
    default     = "monitor"
  }
  ```

- **variable "hostname"**: The hostname for ingress.
  ```hcl
  variable "hostname" {
    description = "Hostname For Ingress"
    type        = string
    default     = "mimir.local"
  }
  ```

- **variable "s3_bucket"**: The S3 bucket for storing Mimir data.
  ```hcl
  variable "s3_bucket" {
    description = "S3 Bucket"
    type        = string
    default     = "default"
  }
  ```

- **variable "s3_endpoint"**: The S3 endpoint URL.
  ```hcl
  variable "s3_endpoint" {
    description = "S3 Endpoint"
    type        = string
    default     = "s3.example.com"
  }
  ```

- **variable "s3_region"**: The S3 region.
  ```hcl
  variable "s3_region" {
    description = "S3 Region"
    type        = string
    default     = ""
  }
  ```

- **variable "s3_accesskey"**: The S3 access key.
  ```hcl
  variable "s3_accesskey" {
    description = "S3 Accesskey"
    type        = string
    default     = ""
  }
  ```

- **variable "s3_secretkey"**: The S3 secret key.
  ```hcl
  variable "s3_secretkey" {
    description = "S3 Secretkey"
    type        = string
    default     = ""
  }
  ```

- **variable "ingress_user"**: The username for ingress access.
  ```hcl
  variable "ingress_user" {
    description = "Ingress User"
    type        = string
    default     = "mimir"
  }
  ```

- **variable "slack_url"**: The URL for Slack notifications.
  ```hcl
  variable "slack_url" {
    type    = string
    default = ""
  }
  ```

- **variable "mimir_metrics_retention_time"**: The period after which old metrics are deleted
```hcl 
variable "mimir_metrics_retention_time" { 
  default = "30d"
}
```

- **variable "loki_credentials"**: Credentials for accessing Loki.
  ```hcl
  variable "loki_credentials" {
    description = "Loki credentials"
    type        = map(string)
    default     = {
      name     = "tenant1"
      password = "secret"
      baseurl  = "http://loki-gateway"
      pushurl  = "http://loki-gateway/loki/api/v1/push"
    }
  }
  ```

## Local Values

Local values used for Helm chart configuration:

- **chart_values**: Configuration values for the Mimir Helm chart.
  ```hcl
  locals {
    chart_values = <<-VALUES
    ...
    VALUES
  }
  ```

- **mimir_resources**: Additional Mimir resources configuration.
  ```hcl
  locals {
    mimir_resources = <<-VALUES
    apiVersion: v1
    ...
    VALUES
  }
  ```

### Example Usage

Here’s an example of how to use the `mimir` module in your Terraform script:

```hcl
module "mimir" {
  source = "../modules/mimir"

  chart_version = "5.2.1"
  namespace     = "monitor"
  hostname      = "mimir.local"
  s3_bucket     = "my-s3-bucket"
  s3_endpoint   = "s3.example.com"
  s3_region     = "us-east-1"
  s3_accesskey  = "my-access-key"
  s3_secretkey  = "my-secret-key"
  ingress_user  = "mimir"
  slack_url     = "https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX"
  loki_credentials = {
    name     = "tenant1"
    password = "secret"
    baseurl  = "http://loki-gateway"
    pushurl  = "http://loki-gateway/loki/api/v1/push"
  }
}
```

## Functions

Grafana Mimir enhances time-series data storage and management capabilities. Key functions include:

- **High Availability and Scalability**: Mimir is designed to be highly available and horizontally scalable for large-scale monitoring systems.
- **Advanced Querying**: Supports advanced querying capabilities with PromQL and integrates seamlessly with Grafana for visualization.
- **Alerting**: Configurable alerting capabilities that integrate with Slack and other notification systems.
- **Storage Integration**: Supports S3-compatible object storage for long-term metrics storage, making it flexible to integrate with different cloud storage providers.
- **Global Rate Limits**: Allows setting global rate limits for ingestions to manage high traffic efficiently.
- **Cache Management**: Provides comprehensive caching capabilities to optimize performance for large datasets.