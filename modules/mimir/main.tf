module "dummy_helm_chart" {
  source = "git::https://gitlab.com/wiregate-public/terraform-modules/dummy-helm-chart"
}

resource "random_password" "ingress_password" {
  length  = 40
  special = false
}


resource "helm_release" "mimir" {
  name             = "mimir"
  repository       = "https://grafana.github.io/helm-charts"
  chart            = "mimir-distributed"
  version          = var.chart_version
  namespace        = var.namespace
  create_namespace = true
  values           = [local.chart_values]

  set {
    name  = "alertmanager.configs.fallback"
    value = "/configs/fallback/fallback.yml"
  }
}

resource "helm_release" "mimir_resources" {
  name             = "mimir-resources"
  chart            = module.dummy_helm_chart.module_path
  namespace        = var.namespace
  create_namespace = true
  depends_on       = [module.dummy_helm_chart]
  set {
    name  = "rawyaml"
    value = base64encode(local.mimir_resources)
  }
}

resource "minio_ilm_policy" "bucket-lifecycle-rules" {
  bucket = "mimir-blocks"

  rule {
    id         = "mimir-blocks-lifecycle"
    expiration = var.mimir_metrics_retention_time
    filter     = "anonymous/"
  }
}
