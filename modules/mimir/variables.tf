variable "chart_version" {
  description = "Chart Version"
  type        = string
  default     = "5.3.0"
}

variable "namespace" {
  description = "Namespace to Install"
  type        = string
  default     = "monitor"
}

variable "hostname" {
  description = "Hostname For Ingress"
  type        = string
  default     = "mimir.local"
}

variable "s3_bucket" {
  description = "S3 Bucket"
  type        = string
  default     = "default"
}

variable "s3_endpoint" {
  description = "S3 Endpoint"
  type        = string
  default     = "s3.example.com"
}

variable "s3_region" {
  description = "S3 Region"
  type        = string
  default     = ""
}

variable "s3_accesskey" {
  description = "S3 Accesskey"
  type        = string
  default     = ""
}

variable "s3_secretkey" {
  description = "S3 Secretkey"
  type        = string
  default     = ""
}

variable "ingress_user" {
  description = "Ingress User"
  type        = string
  default     = "mimir"
}

variable "slack_url" {
  default = ""
}

variable "mimir_metrics_retention_time" {
  default = "30d"
}

variable "loki_credentials" {
  description = "Loki credentials"
  type        = map(string)
  default = {
    name     = "tenant1"
    password = "secret"
    baseurl  = "http://loki-gateway"
    pushurl  = "http://loki-gateway/loki/api/v1/push"
  }
}

locals {
  chart_values    = <<-VALUES
    mimir:
      structuredConfig:
        limits:
          ingestion_rate: 100000
          ingestion_burst_size: 2000000
          ruler_max_rules_per_rule_group: 200
          max_global_series_per_user: 0
          compactor_blocks_retention_period: ${var.mimir_metrics_retention_time}
          compactor_partial_block_deletion_delay: 4h
          out_of_order_time_window: 10m 
        common:
          storage:
            backend: s3
            s3:
              endpoint: ${var.s3_endpoint}
              region: us-east
              secret_access_key: ${var.s3_secretkey}
              access_key_id: ${var.s3_accesskey}
        blocks_storage:
          s3:
            bucket_name: mimir-blocks
        alertmanager_storage:
          s3:
            bucket_name: mimir-alertmanager
        ruler_storage:
          s3:
            bucket_name: mimir-ruler

    alertmanager:
      persistentVolume:
        enabled: true
      replicas: 1
      resources:
        limits:
          memory: 1Gi
        requests:
          cpu: 100m
          memory: 128Mi
      statefulSet:
        enabled: true
      fallbackConfig: |       
        global:
          resolve_timeout: 5m
        templates:
          - default_template
        route:
          group_by: ['alertname', 'job']
          group_wait: 30s
          group_interval: 5m
          repeat_interval: 1h
          receiver: slack-notifications
          routes:
            - match:
                severity: critical
              receiver: slack-notifications
        receivers:
          - name: slack-notifications
            slack_configs:
              - send_resolved: true
                text: "{{ range .Alerts }}{{ .Annotations.description }}\n{{ end }}"
                api_url: ${var.slack_url} 
                channel: #alerts

    compactor:
      persistentVolume:
        size: 10Gi
      resources:
        limits:
          memory: 2.1Gi
        requests:
          cpu: 100m
          memory: 512Mi

    distributor:
      replicas: 1
      resources:
        limits:
          memory: 4096Mi
        requests:
          cpu: 500m
          memory: 512Mi

    ingester:
      persistentVolume:
        size: 25Gi
      replicas: 2
      resources:
        limits:
          memory: 6Gi
        requests:
          cpu: 1
          memory: 2Gi
      topologySpreadConstraints: {}
      affinity: {}
      zoneAwareReplication:
        enabled: false

    admin-cache:
      enabled: true
      replicas: 1

    chunks-cache:
      enabled: true
      replicas: 1
      allocatedMemory: 4096

    index-cache:
      enabled: true
      replicas: 1

    metadata-cache:
      enabled: true

    results-cache:
      enabled: true
      replicas: 1

    minio:
      enabled: false

    overrides_exporter:
      replicas: 1
      resources:
        limits:
          memory: 128Mi
        requests:
          cpu: 100m
          memory: 128Mi

    querier:
      replicas: 1
      resources:
        limits:
          memory: 4Gi
        requests:
          cpu: 1
          memory: 2Gi

    query_frontend:
      replicas: 1
      resources:
        limits:
          memory: 3Gi
        requests:
          cpu: 1
          memory: 2Gi

    query_scheduler:
      enabled: true
      replicas: 1
      resources:
        requests:
          cpu: 100m
          memory: 128Mi

    ruler:
      replicas: 1
      resources:
        limits:
          memory: 4Gi
        requests:
          cpu: 1
          memory: 2Gi

    store_gateway:
      persistentVolume:
        size: 10Gi
      replicas: 1
      resources:
        limits:
          memory: 2Gi
        requests:
          cpu: 1
          memory: 1536Mi
      topologySpreadConstraints: {}
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: target # support for enterprise.legacyLabels
                operator: In
                values:
                - store-gateway
            topologyKey: 'kubernetes.io/hostname'
          - labelSelector:
              matchExpressions:
              - key: app.kubernetes.io/component
                operator: In
                values:
                - store-gateway
            topologyKey: 'kubernetes.io/hostname'
      zoneAwareReplication:
        enabled: false

    nginx:
      replicas: 1
      resources:
        limits:
          memory: 768Mi
        requests:
          cpu: 100m
          memory: 128Mi
      ingress:
        enabled: true
        ingressClassName: nginx
        annotations:
          ingress.kubernetes.io/ssl-redirect: "true"
          kubernetes.io/tls-acme: "true"
        hosts:
        - host: ${var.hostname}
          paths:
          - path: /
            pathType: Prefix
        tls:
        - hosts:
          - ${var.hostname}
          secretName: ${replace(var.hostname, ".", "-")}-tls
      basicAuth:
        enabled: true
        username: ${var.ingress_user}
        password: ${resource.random_password.ingress_password.result}
    metaMonitoring:
      dashboards:
        enabled: true
      serviceMonitor:
        enabled: true
      grafanaAgent:
        enabled: true
        logs:
          remote:
            url: ${var.loki_credentials.pushurl}
            auth:
              username: ${var.loki_credentials.name}
              passwordSecretName: mimir-loki-password
              passwordSecretKey: password
        metrics:
          remote:
            auth:
              username: ${var.ingress_user}
              passwordSecretName: mimir-metrics-password
              passwordSecretKey: password
    VALUES
  mimir_resources = <<-VALUES
    apiVersion: v1
    kind: Secret
    metadata:
      name: mimir-loki-password
    type: Opaque
    data:
      password: ${base64encode(var.loki_credentials.password)}
    ---
    apiVersion: v1
    kind: Secret
    metadata:
      name: mimir-metrics-password
    type: Opaque
    data:
      password: ${base64encode(resource.random_password.ingress_password.result)}
    VALUES
}
