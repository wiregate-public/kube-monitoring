output "credentials" {
  value = {
    pushurl  = "https://${var.hostname}/api/v1/push"
    baseurl  = "https://${var.hostname}"
    name     = var.ingress_user
    password = resource.random_password.ingress_password.result
  }
  sensitive = true
}

output "datasource" {
  value = {
    name          = "Mimir"
    type          = "prometheus"
    access        = "proxy"
    url           = "http://mimir-nginx/prometheus"
    basicAuth     = true
    basicAuthUser = var.ingress_user
    secureJsonData = {
      basicAuthPassword = resource.random_password.ingress_password.result
    }
  }
}
