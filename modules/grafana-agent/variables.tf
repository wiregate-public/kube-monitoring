variable "chart_version" {
  description = "Chart Version"
  type        = string
  default     = "0.41.0"
}

variable "namespace" {
  description = "Namespace to Install"
  type        = string
  default     = "monitor"
}

variable "mimir_credentials" {
  description = "Mimir credentials"
  type        = map(string)
  default = {
    name     = "tenant1"
    password = "secret"
    baseurl  = "http://example.com"
    pushurl  = "http://example.com/api/v1/push"
  }
}

variable "loki_credentials" {
  description = "Loki credentials"
  type        = map(string)
  default = {
    name     = "tenant1"
    password = "secret"
    baseurl  = "http://example.com"
    pushurl  = "http://loki-gateway/loki/api/v1/push"
  }
}

variable "cluster_name" {
  description = "Cluster Name"
  type        = string
  default     = "kubernetes"
}

locals {
  metrics_chart_values = <<-VALUES
    agent:
      mode: 'flow'
      resources:
        requests:
          memory: "256Mi"
          cpu: "50m"
        limits:
          memory: "1024Mi"
          cpu: "500m"      
      configMap:
        create: true
        content: |
          prometheus.remote_write "default" {
              endpoint {
                  url = "${var.mimir_credentials.pushurl}"
                  basic_auth {
                      username = "${var.mimir_credentials.name}"
                      password = "${var.mimir_credentials.password}"
                  }
              }
          }

          discovery.kubernetes "nodes" {
              role = "node"
          }

          prometheus.operator.servicemonitors "services" {
              forward_to = [prometheus.remote_write.default.receiver]
              clustering {
                  enabled = true
              }
          }

          discovery.relabel "metrics_cadvisor" {
              targets = discovery.kubernetes.nodes.targets
              rule {
                  action = "replace"
                  target_label = "cluster"
                  replacement = "${var.cluster_name}"
              }
              rule {
                  action = "replace"
                  target_label = "__address__"
                  replacement = "kubernetes.default.svc.cluster.local:443"
              }
              rule {
                  source_labels = ["__meta_kubernetes_node_name"]
                  regex = "(.+)"
                  action = "replace"
                  replacement = "/api/v1/nodes/$${1}/proxy/metrics/cadvisor"
                  target_label = "__metrics_path__"
              }
          }

          prometheus.scrape "cadvisor" {
              scheme = "https"
              tls_config {
                  server_name = "kubernetes"
                  ca_file = "/var/run/secrets/kubernetes.io/serviceaccount/ca.crt"
                  insecure_skip_verify = false
              }
              clustering {
                  enabled = true
              }
              bearer_token_file = "/var/run/secrets/kubernetes.io/serviceaccount/token"
              targets = discovery.relabel.metrics_cadvisor.output
              scrape_interval = "30s"
              forward_to = [prometheus.relabel.cadvisor.receiver]
          }

          prometheus.relabel "cadvisor" {
              forward_to = [prometheus.remote_write.default.receiver]
              rule {
                  action = "replace"
                  target_label = "job"
                  replacement = "kubelet"
              }
              rule {
                  action = "replace"
                  target_label = "metrics_path"
                  replacement = "/metrics/cadvisor"
              }
          }

          discovery.relabel "metrics_kubelet" {
              targets = discovery.kubernetes.nodes.targets
              rule {
                  action = "replace"
                  target_label = "cluster"
                  replacement = "${var.cluster_name}"
              }
              rule {
                  action = "replace"
                  target_label = "__address__"
                  replacement = "kubernetes.default.svc.cluster.local:443"
              }
              rule {
                  source_labels = ["__meta_kubernetes_node_name"]
                  regex = "(.+)"
                  action = "replace"
                  replacement = "/api/v1/nodes/$${1}/proxy/metrics"
                  target_label = "__metrics_path__"
              }
          }

          prometheus.scrape "kubelet" {
              scheme = "https"
              tls_config {
                  server_name = "kubernetes"
                  ca_file = "/var/run/secrets/kubernetes.io/serviceaccount/ca.crt"
                  insecure_skip_verify = false
              }
              clustering {
                  enabled = true
              }
              bearer_token_file = "/var/run/secrets/kubernetes.io/serviceaccount/token"
              targets = discovery.relabel.metrics_kubelet.output
              scrape_interval = "15s"
              forward_to = [prometheus.relabel.kubelet.receiver]
          }

          prometheus.relabel "kubelet" {
              forward_to = [prometheus.remote_write.default.receiver]
              rule {
                  action = "replace"
                  target_label = "job"
                  replacement = "kubelet"
              }
              rule {
                  action = "replace"
                  target_label = "metrics_path"
                  replacement = "/metrics"
              }
              rule {
                  action = "drop"
                  source_labels = ["__name__"]
                  regex = "kubeproxy_.*|apiserver_.*"
              }
          }
          ## Application metrics (with laravel-prometheus module)  
          
          # discovery.relabel "metrics_php" {
          #   targets = [
          #     {
          #       __address__ = "frontend.prod.svc.cluster.local:80",
          #     },
          #   ]
          #   rule {
          #     action = "replace"
          #     target_label = "__metrics_path__"
          #     replacement = "/prometheus"
          #   }
          #   rule {
          #     action = "replace"
          #     target_label = "cluster"
          #     replacement = "${var.cluster_name}"
          #   }
          # }
          
          # prometheus.scrape "php_metrics" {
          #     scheme = "http"
          #     targets = discovery.relabel.metrics_php.output
          #     scrape_interval = "60s"
          #     forward_to = [prometheus.relabel.php_metrics.receiver]
          # }

          # prometheus.relabel "php_metrics" {
          #   forward_to = [prometheus.remote_write.default.receiver]
          #   rule {
          #     action = "replace"
          #     target_label = "job"
          #     replacement = "laravel_metrics"
          #   }
          # }
          
      clustering:
        enabled: true
    controller:
      type: 'statefulset'
      replicas: 2     
    crds:
      create: false
    VALUES

  logs_chart_values = <<-VALUES
    agent:
      mode: 'flow'
      resources:
        requests:
          memory: "64Mi"
          cpu: "30m"
        limits:
          memory: "512Mi"
          cpu: "400m"
      configMap:
        create: true
        content: |
          discovery.kubernetes "pods" {
            role = "pod"
            // limit to pods on this node to reduce the amount you need to filter
            selectors {
              role  = "pod"
              field = "spec.nodeName=" + env("HOSTNAME")
            }
          }

          discovery.relabel "pod_logs" {
            targets = discovery.kubernetes.pods.targets
            rule {
              source_labels = ["__meta_kubernetes_namespace"]
              target_label  = "namespace"
            }
            rule {
              source_labels = ["__meta_kubernetes_pod_name"]
              target_label  = "pod"
            }
            rule {
              source_labels = ["__meta_kubernetes_pod_container_name"]
              target_label  = "container"
            }
            rule {
              source_labels = ["__meta_kubernetes_namespace", "__meta_kubernetes_pod_name"]
              separator     = "/"
              target_label  = "job"
            }
            rule {
              source_labels = ["__meta_kubernetes_pod_uid", "__meta_kubernetes_pod_container_name"]
              separator     = "/"
              action        = "replace"
              replacement   = "/var/log/pods/*$1/*.log"
              target_label  = "__path__"
            }
          }

          local.file_match "pod_logs" {
            path_targets = discovery.relabel.pod_logs.output
          }

          loki.source.file "pod_logs" {
            targets    = local.file_match.pod_logs.targets
            forward_to = [loki.relabel.add_cluster_label.receiver]
          }

          loki.relabel "add_cluster_label" {
            forward_to = [loki.process.pod_logs.receiver]
            rule {
                target_label = "cluster"
                replacement  = "${var.cluster_name}"
            }
          }

          // basic processing to parse the container format. You can add additional processing stages
          // to match your application logs.
          loki.process "pod_logs" {
            stage.match {
              selector = "{tmp_container_runtime=\"containerd\"}"
              // the cri processing stage extracts the following k/v pairs: log, stream, time, flags
              stage.cri {}
              // Set the extract flags and stream values as labels
              stage.labels {
                values = {
                  flags   = "",
                  stream  = "",
                }
              }
            }

            // if the label tmp_container_runtime from above is docker parse using docker
            stage.match {
              selector = "{tmp_container_runtime=\"docker\"}"
              // the docker processing stage extracts the following k/v pairs: log, stream, time
              stage.docker {}

              // Set the extract stream value as a label
              stage.labels {
                values = {
                  stream  = "",
                }
              }
            }

            // drop the temporary container runtime label as it is no longer needed
            stage.label_drop {
              values = ["tmp_container_runtime"]
            }
            forward_to = [loki.write.loki.receiver]
          }

          loki.source.kubernetes_events "events" {
            log_format = "json"
            forward_to = [loki.write.loki.receiver]
          }

          loki.write "loki" {
            endpoint {
              url = "${var.loki_credentials.pushurl}"
              basic_auth {
                username = "${var.loki_credentials.name}"
                password = "${var.loki_credentials.password}"
              }
            }
          }
      clustering:
        enabled: false
      mounts:
        varlog: true
    controller:
      type: 'daemonset'
    VALUES

  ruler_chart_values = <<-VALUES
    agent:
      mode: 'flow'
      resources:
        requests:
          memory: "64Mi"
          cpu: "30m"
        limits:
          memory: "512Mi"
          cpu: "400m"      
      configMap:
        create: true
        content: |
          mimir.rules.kubernetes "default" {
              address = "${var.mimir_credentials.baseurl}"
              basic_auth {
                  username = "${var.mimir_credentials.name}"
                  password = "${var.mimir_credentials.password}"
              }
          }
      clustering:
        enabled: false
    controller:
      type: 'statefulset'
      replicas: 1
    crds:
      create: false
    VALUES
}
