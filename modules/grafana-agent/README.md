# grafana-agent Module

The `grafana-agent` module is designed to deploy Grafana Agent into your Kubernetes cluster. Grafana Agent is a telemetry collector optimized for sending metrics, logs, and tracing data to Grafana server, Grafana Cloud or other destination. This module uses Helm chart `grafana-agent`from repository [https://grafana.github.io/helm-charts](https://grafana.github.io/helm-charts) for deployment.

## Overview

Grafana Agent is an efficient and lightweight telemetry collector that integrates tightly with the Grafana ecosystem. It supports various data collection protocols and can be configured to forward collected data to different backends such as Prometheus, Loki, and Tempo.

## Resources Created by the Module

The module creates the following resources:

- **resource "helm_release" "grafana_agent_crds"**: Deploys the CRDs required for Grafana Agent.

- **resource "helm_release" "grafana_agent_metrics"**: Deploys the Grafana Agent configured for metrics collection.

- **resource "helm_release" "grafana_agent_logs"**: Deploys the Grafana Agent configured for log collection.

- **resource "helm_release" "grafana_agent_ruler"**: Deploys the Grafana Agent configured for rule evaluation.


## Variables

The module accepts the following parameters:

- **variable "chart_version"**: The version of the Grafana Agent Helm chart to deploy.
  ```hcl
  variable "chart_version" {
    description = "Chart Version"
    type        = string
    default     = "0.31.1"
  }
  ```

- **variable "namespace"**: The namespace in which to install the Helm releases.
  ```hcl
  variable "namespace" {
    description = "Namespace to Install"
    type        = string
    default     = "monitor"
  }
  ```

- **variable "mimir_credentials"**: Credentials for connecting to Mimir for metrics.
  ```hcl
  variable "mimir_credentials" {
    description = "Mimir credentials"
    type        = map(string)
    default     = {
      name     = "tenant1"
      password = "secret"
      baseurl  = "http://example.com"
      pushurl  = "http://example.com/api/v1/push"
    }
  }
  ```

- **variable "loki_credentials"**: Credentials for connecting to Loki for logs.
  ```hcl
  variable "loki_credentials" {
    description = "Loki credentials"
    type        = map(string)
    default     = {
      name     = "tenant1"
      password = "secret"
      baseurl  = "http://example.com"
      pushurl  = "http://loki-gateway/loki/api/v1/push"
    }
  }
  ```

- **variable "cluster_name"**: The name of the Kubernetes cluster.
  ```hcl
  variable "cluster_name" {
    description = "Cluster Name"
    type        = string
    default     = "kubernetes"
  }
  ```

## Local Values

Local values used for Helm chart configurations:

- **metrics_chart_values**: Configuration values for the Grafana Agent metrics.
  ```hcl
  locals {
    metrics_chart_values = <<-VALUES
    ...
    VALUES
  }
  ```

- **logs_chart_values**: Configuration values for the Grafana Agent logs.
  ```hcl
  locals {
    logs_chart_values = <<-VALUES
    ...
    VALUES
  }
  ```

- **ruler_chart_values**: Configuration values for the Grafana Agent ruler.
  ```hcl
  locals {
    ruler_chart_values = <<-VALUES
    ...
    VALUES
  }
  ```

### Example Usage

Here’s an example of how to use the `grafana-agent` module in your Terraform script:

```hcl
module "grafana_agent" {
  source = "../modules/grafana_agent"

  chart_version     = "0.31.1"
  namespace         = "monitor"
  mimir_credentials = {
    name     = "tenant1"
    password = "secret"
    baseurl  = "http://example.com"
    pushurl  = "http://example.com/api/v1/push"
  }
  loki_credentials = {
    name     = "tenant1"
    password = "secret"
    baseurl  = "http://example.com"
    pushurl  = "http://loki-gateway/loki/api/v1/push"
  }
  cluster_name = "kubernetes"
}
```

## Functionality

Grafana Agent performs several crucial functions within a Kubernetes cluster:

1. **Metrics Collection**: Grafana Agent collects metrics from Kubernetes nodes, kubelets, and other components and forwards them to a backend like Mimir.
2. **Log Collection**: Grafana Agent collects logs from Kubernetes pods and forwards them to a backend like Loki for aggregation and analysis.
3. **Rule Evaluation**: Grafana Agent can evaluate Prometheus-style alerting and recording rules and forward results to a rules storage backend.
4. **Cluster Monitoring**: By integrating with different data sources and relabeling rules, it enables comprehensive monitoring and observability capabilities for Kubernetes clusters.

Grafana Agent is optimized for minimizing resource consumption and simplifying observability configuration in Kubernetes environments.