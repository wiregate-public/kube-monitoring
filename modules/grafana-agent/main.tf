resource "helm_release" "grafana_agent_crds" {
  name             = "grafana-agent-crds"
  chart            = "${path.module}/crds-chart"
  version          = var.chart_version
  namespace        = var.namespace
  create_namespace = true
}

resource "helm_release" "grafana_agent_metrics" {
  name             = "grafana-agent-metrics"
  repository       = "https://grafana.github.io/helm-charts"
  chart            = "grafana-agent"
  version          = var.chart_version
  namespace        = var.namespace
  create_namespace = true
  values           = [local.metrics_chart_values]
  depends_on       = [resource.helm_release.grafana_agent_crds]
}

resource "helm_release" "grafana_agent_logs" {
  name             = "grafana-agent-logs"
  repository       = "https://grafana.github.io/helm-charts"
  chart            = "grafana-agent"
  version          = var.chart_version
  namespace        = var.namespace
  create_namespace = true
  values           = [local.logs_chart_values]
  depends_on       = [resource.helm_release.grafana_agent_crds]
}

resource "helm_release" "grafana_agent_ruler" {
  name             = "grafana-agent-ruler"
  repository       = "https://grafana.github.io/helm-charts"
  chart            = "grafana-agent"
  version          = var.chart_version
  namespace        = var.namespace
  create_namespace = true
  values           = [local.ruler_chart_values]
  depends_on       = [resource.helm_release.grafana_agent_crds]
}
