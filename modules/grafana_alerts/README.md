# grafana_alerts Module

The `grafana_alerts` module creates resources for configuring alerts in Grafana, linked to various contact points such as email, Telegram, and Slack. This module uses the Terraform provider `grafana/grafana`.

## Resources Created by the Module

The module creates the following resources:

- **data "grafana_data_source" "mimir"**: Retrieves the UID of the Mimir datasource.

- **resource "grafana_contact_point" "slack_cp_critical"**: Configures a Slack contact point for critical alerts.

- **resource "grafana_contact_point" "slack_cp_warning"**: Configures a Slack contact point for warning alerts.

- **resource "grafana_contact_point" "mail_cp"**: Configures an email contact point.

- **resource "grafana_notification_policy" "new_notification_policy"**: Configures notification policies for alerts.  In this resource, you can set intervals and other notification parameters.

- **resource "grafana_folder" "rule_folder"**: Creates a folder to organize alert rules.

- **resource "grafana_rule_group" "rule_group"**: Creates a group of alert rules using custom rules defined in `rules.json`.


## Variables

The module accepts the following parameters:

- **variable "slack_url"**: The Slack Webhook URL for sending alerts.
  ```hcl
  variable "slack_url" {
    description = "Slack Webhook URL"
    default     = ""
  }
  ```

- **variable "telegram_token"**: The Telegram token used to send alerts to Telegram.
  ```hcl
  variable "telegram_token" {
    description = "Telegram Token"
    default     = ""
  }
  ```

- **variable "telegram_chatid"**: The Telegram chat ID where alerts should be sent.
  ```hcl
  variable "telegram_chatid" {
    description = "Telegram Chat ID"
    default     = ""
  }
  ```

## Custom Alert Rules

This module allows you to define custom Grafana alert rules in the `rules.json` file, placed in the module's root directory. These rules are dynamically loaded and applied to the Grafana instance.

## Example Usage

Here’s an example of how to use the `grafana_alerts` module in your Terraform script:

```hcl
module "grafana_alerts" {
  source = "../modules/grafana_alerts"

  slack_url = "https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX"
}
```

## Functionality

This module enables robust alerting capability in Grafana by providing pre-configured contact points for different communication channels such as email, Slack, and Telegram (commented out and not included by default). Notification policies help to categorize and route alerts based on severity and other labels.

Alerts are routed as follows:
- **Critical Alerts**: Sent to Slack channel `#alerts` with a "rotating light" emoji.
- **Warning Alerts**: Sent to Slack channel `#warnings` with a "warning" emoji.
- **Email Alerts**: All alerts are summarized and sent to the specified email addresses.

Custom alert rules are defined in `rules.json` and applied to the Grafana instance, offering fine-grained control over alert conditions, thresholds, and notifications.