data "grafana_data_source" "mimir" {
  name = "Mimir"  # default datasource
}

output "default_datasource_uid" {
  value = data.grafana_data_source.mimir.uid
}

resource "grafana_contact_point" "slack_cp_critical" {
  name = "slack_cp_critical"
  slack {
    disable_resolve_message = false
    text                    = "{{ range .Alerts }}{{ .Annotations.description }}\n{{ end }}"
    title                   = "Grafana alert"
    url                     = var.slack_url
    icon_emoji              = ":rotating_light:" 
    recipient               = "#alerts"
  }
}

resource "grafana_contact_point" "slack_cp_warning" {
  name = "slack_cp_warning"
  slack {
    disable_resolve_message = true
    text                    = "{{ range .Alerts }}{{ .Annotations.description }}\n{{ end }}"
    title                   = "Grafana alert"
    url                     = var.slack_url
    icon_emoji              = ":warning:" 
    recipient               = "#warnings"
  }
}

resource "grafana_contact_point" "mail_cp" {
  name = "mail contact point"
  email {
    addresses               = ["one@company.org"]
    message                 = "{{ len .Alerts.Firing }} firing."
    subject                 = "{{ template \"default.title\" .}}"
    single_email            = true
    disable_resolve_message = false
  }
}

resource "grafana_notification_policy" "new_notification_policy" {
  group_by      = ["severity"]
  contact_point = grafana_contact_point.mail_cp.name

  policy {
    matcher {
      label = "severity"
      match = "="
      value = "critical"
    }
    group_wait      = "45s"
    group_interval  = "6m"
    repeat_interval = "12h"

    group_by      = ["alertname"]    
    contact_point = grafana_contact_point.slack_cp_critical.name
    continue      = true
  }
  policy {
    matcher {
      label = "severity"
      match = "!="
      value = "critical"
    }
    group_wait      = "1m"
    group_interval  = "10m"
    repeat_interval = "24h"
    contact_point = grafana_contact_point.slack_cp_warning.name
    group_by      = ["alertname"]
  }
}

resource "grafana_folder" "rule_folder" {
  title = "grafana_rules_folder"
}


locals {
  rules = jsondecode(file("${path.module}/rules.json")).rules
}

resource "grafana_rule_group" "rule_group" {
  org_id           = 1
  name             = "hardware_alerts_group"
  folder_uid       = grafana_folder.rule_folder.uid
  interval_seconds = 300

  dynamic "rule" {
    for_each = local.rules

    content {
      name      = rule.value.name
      condition = "B"

      data {
        ref_id = "A"

        relative_time_range {
          from = 600
          to   = 0
        }

        datasource_uid = data.grafana_data_source.mimir.uid
        model          = jsonencode({
          "editorMode": "code",
          "expr": rule.value.expr,
          "instant": true,
          "intervalMs": 1000,
          "legendFormat": "__auto",
          "maxDataPoints": 43200,
          "range": false,
          "refId": "A"
        })
      }

      data {
        ref_id = "B"

        relative_time_range {
          from = 600
          to   = 0
        }

        datasource_uid = "__expr__"
        model          = jsonencode({
          "conditions": [{
            "evaluator": {
              "params": [rule.value.threshold],
              "type": rule.value.type
            },
            "operator": {
              "type": "and"
            },
            "query": {
              "params": ["A"]  // link to the first data block
            },
            "reducer": {
              "params": [],
              "type": "last"
            },
            "type": "query"
          }],
          "datasource": {
            "type": "__expr__",
            "uid": "__expr__"
          },
          "expression": "A",
          "intervalMs": 1000,
          "maxDataPoints": 43200,
          "refId": "B",
          "type": "threshold"
        })
      }

      no_data_state  = "NoData"
      exec_err_state = "Error"
      for            = "5m"

      annotations = {
        summary     = "Alert for ${rule.value.name}"
        description = rule.value.description
      }

      labels = {
        severity = rule.value.severity
      }
      is_paused = false
    }
  }
}