variable "grafana_auth" {
  default = "admin:Password"
}

variable "def_datasource" {}

variable "slack_url" {
    default = ""
}

variable "telegram_token" {
    default = ""
}

variable "telegram_chatid" {
    default = ""
}