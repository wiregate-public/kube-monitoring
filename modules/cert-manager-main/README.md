# Cert-Manager


This modules installs:
 - Cert-Manager Custom Resource Definitions (CRDs);
 - Cert-Manager helm chart;
 - Cert-Manager default cluster issuer;
