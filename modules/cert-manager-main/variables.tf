variable "cert_manager_chart_version" {
  description = "Cert Manager chart version"
  type        = string
  default     = "1.13.3"
}

variable "cert_manager_email" {
  description = "Email for certificate notifications"
  type        = string
  default     = "devops@example.com"
}

locals {
  chart_values = {
    ingressShim = {
      defaultIssuerKind = "ClusterIssuer"
      defaultIssuerName = "cert-manager-default-clusterissuer"
    }
    maxConcurrentChallenges = "300"
  }
  cert_manager_default_cluster_issuer = {
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "ClusterIssuer"
    "metadata" = {
      "name" = "cert-manager-default-clusterissuer"
    }
    "spec" = {
      "acme" = {
        "email" = "${var.cert_manager_email}"
        "privateKeySecretRef" = {
          "name" = "letsencrypt-key-issuer-prod"
        }
        "server" = "https://acme-staging-v02.api.letsencrypt.org/directory"
        "solvers" = [
          {
            "http01" = {
              "ingress" = {
                "class" = "nginx"
              }
            }
          }
        ]
      }
    }
  }
}
