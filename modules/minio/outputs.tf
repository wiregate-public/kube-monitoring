output "root_user" {
  description = "Minio Root User"
  value       = var.root_user
  sensitive   = false
}

output "root_password" {
  description = "Minio Root Password"
  value       = resource.random_password.root_password.result
  sensitive   = true
}

output "accesskey" {
  description = "Minio Access Key"
  value       = var.accesskey
  sensitive   = false
}

output "secretkey" {
  description = "Minio Secret Key"
  value       = resource.random_password.secretkey.result
  sensitive   = true
}
