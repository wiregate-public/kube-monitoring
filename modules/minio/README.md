# minio Module

The `minio` module deploys MinIO, a high-performance object storage system, into your Kubernetes cluster using the `minio` Helm chart from the repository [https://charts.min.io](https://charts.min.io).

## Overview

MinIO is an open-source object storage server compatible with the S3 API. It is designed for large-scale data infrastructure, providing high performance and strong consistency. In this project, MinIO is used in conjunction with Mimir for efficient storage and retrieval of metric data.

## Resources Created by the Module

The module creates the following resources:

- **resource "random_password" "root_password"**: Generates a random password for the MinIO root user.

- **resource "random_password" "secretkey"**: Generates a random secret key for MinIO.

- **resource "helm_release" "minio"**: Installs the MinIO Helm chart.


## Variables

The module accepts the following parameters:

- **variable "chart_version"**: The version of the MinIO Helm chart to deploy.
  ```hcl
  variable "chart_version" {
    description = "Chart Version"
    type        = string
    default     = "5.0.15"
  }
  ```

- **variable "namespace"**: The namespace in which to install the Helm release.
  ```hcl
  variable "namespace" {
    description = "Namespace to Install"
    type        = string
    default     = "minio"
  }
  ```

- **variable "root_user"**: The root user for MinIO.
  ```hcl
  variable "root_user" {
    description = "Minio Root User"
    type        = string
    default     = "admin"
  }
  ```

- **variable "hostname"**: The hostname for MinIO.
  ```hcl
  variable "hostname" {
    description = "Minio Hostname"
    type        = string
    default     = "minio.local"
  }
  ```

## Local Values

Local values used for Helm chart configuration:

- **chart_values**: Configuration values for the MinIO Helm chart.
  ```hcl
  locals {
    chart_values = <<-VALUES
    ...
    VALUES
  }
  ```

### Example Usage

Here’s an example of how to use the `minio` module in your Terraform script:

```hcl
module "minio" {
  source     = "../modules/minio"
  chart_version = "5.0.15"
  namespace     = "minio"
  root_user     = "admin"
  hostname      = "minio.local"
}
```

## Functionality

MinIO provides high-performance, S3-compatible object storage for a variety of use cases including:
- **Data Lake**: Store massive amounts of unstructured data.
- **Backup and Restore**: Efficiently backup and restore data with high reliability.
- **Big Data Analytics**: Serve as an object storage backend for data analytics frameworks.

In this project, MinIO is used in conjunction with Mimir to store and retrieve metrics data, providing a robust and scalable solution for metrics collection and analysis.