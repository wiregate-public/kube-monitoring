variable "chart_version" {
  description = "Chart Version"
  type        = string
  default     = "5.0.15"
}

variable "namespace" {
  description = "Namespace to Install"
  type        = string
  default     = "minio"
}

variable "root_user" {
  description = "Minio Root User"
  type        = string
  default     = "admin"
}

variable "hostname" {
  description = "Minio Hostname"
  type        = string
  default     = "minio.local"
}

variable "console_hostname" {
  description = "Minio Console Hostname"
  type        = string
  default     = "s3-console.minio.local"
}

variable "accesskey" {
  description = "Minio Bucket AccessKey"
  type        = string
  default     = "minio"
}

variable "persistance_size" {
  description = "Minio Bucket Persistance Size"
  type        = string
  default     = "10Gi"
}

variable "buckets" {
  description = "Minio Buckets"
  type        = list(string)
  default     = ["default"]
}

locals {
  chart_values = <<-VALUES
    buckets:
    ${join("\n", [for bucket_name in var.buckets : <<-EOT
    - name: ${bucket_name}
      policy: none
      purge: false
    EOT
])}
    ingress:
      enabled: true
      ingressClassName: nginx
      annotations:
        ingress.kubernetes.io/ssl-redirect: "true"
        kubernetes.io/tls-acme: "true"
        nginx.ingress.kubernetes.io/proxy-body-size: 10g
      hosts:
      - ${var.hostname}
      path: /
      tls:
      - hosts:
        - ${var.hostname}
        secretName: ${replace(var.hostname, ".", "-")}-tls
    consoleIngress:
      enabled: true
      ingressClassName: nginx
      annotations:
        cert-manager.io/cluster-issuer: cert-manager-default-clusterissuer
        nginx.ingress.kubernetes.io/proxy-body-size: 10g
      path: /
      hosts:
       - ${var.console_hostname}
      tls:
      - hosts:
        - ${var.console_hostname}
        secretName: ${replace(var.console_hostname, ".", "-")}-tls        
    mode: standalone
    persistence:
      size: ${var.persistance_size}
      ### TEMPORARY COMMENTED FOR TESTING PURPOSES (ALLOWED TO DELETE RESOURCE)
      # annotations:
      #   "helm.sh/resource-policy": keep
    policies:
    - name: defaultrw
      statements:
      - actions:
        - s3:GetBucketLocation
        - s3:GetObject
        - s3:ListBucket
        - s3:ListMultipartUploadParts
        - s3:PutObject
        - s3:AbortMultipartUpload
        - s3:DeleteObject
        resources:
        ${join("\n    ", [for bucket_name in var.buckets : "- arn:aws:s3:::${bucket_name}/*"])}
    replicas: 1
    resources:
      limits:
        cpu: "2"
        memory: 4096Mi
      requests:
        cpu: 500m
        memory: 512Mi
    rootPassword: ${resource.random_password.root_password.result}
    rootUser: ${var.root_user}
    users:
    - accessKey: ${var.accesskey}
      policy: defaultrw
      secretKey: ${resource.random_password.secretkey.result}
    VALUES
}
