resource "random_password" "root_password" {
  length  = 20
  special = false
}

resource "random_password" "secretkey" {
  length  = 20
  special = false
}

resource "helm_release" "minio" {
  name             = "minio"
  repository       = "https://charts.min.io/"
  chart            = "minio"
  version          = var.chart_version
  namespace        = var.namespace
  create_namespace = true
  values           = [local.chart_values]
}
