output "credentials" {
  value = {
    pushurl  = "http://loki-gateway.${var.namespace}/loki/api/v1/push"
    baseurl  = "http://loki-gateway.${var.namespace}"    
    name     = var.tenant_name
    password = resource.random_password.tenant1.result
  }
  sensitive = true
}

output "var_credentials" {
  value = {
    pushurl  = "https://${var.hostname}/loki/api/v1/push"
    baseurl  = "https://${var.hostname}"    
    name     = var.tenant_name
    password = resource.random_password.tenant1.result
  }
  sensitive = true
}

output "datasource" {
  value = {
    name          = "Loki"
    type          = "loki"
    access        = "proxy"
    url           = "http://loki-gateway"
    basicAuth     = true
    basicAuthUser = var.tenant_name
    jsonData = {
      httpHeaderName1 = "X-Scope-OrgID"
    }
    secureJsonData = {
      basicAuthPassword = resource.random_password.tenant1.result
      httpHeaderValue1  = var.tenant_name
    }
  }
}
