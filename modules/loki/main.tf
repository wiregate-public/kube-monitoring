module "dummy_helm_chart" {
  source = "git::https://gitlab.com/wiregate-public/terraform-modules/dummy-helm-chart"
}

resource "random_password" "tenant1" {
  length  = 20
  special = false
}

resource "random_password" "self_monitoring" {
  length  = 20
  special = false
}

resource "helm_release" "loki" {
  name             = var.release_name
  repository       = "https://grafana.github.io/helm-charts"
  chart            = "loki"
  version          = var.chart_version
  namespace        = var.namespace
  create_namespace = true
  values           = [local.chart_values]
}

resource "helm_release" "loki_resources" {
  name             = "loki-resources"
  chart            = module.dummy_helm_chart.module_path
  namespace        = var.namespace
  create_namespace = true
  depends_on       = [module.dummy_helm_chart]
  set {
    name  = "rawyaml"
    value = base64encode(local.loki_resources)
  }
}
