# loki Module

The `loki` module deploys Grafana Loki, a log aggregation system designed to store and query logs from various services. This module uses Helm charts for deployment.

## Overview

Grafana Loki is a log aggregation system that is cost-effective, efficient, and highly available, designed specifically to work with Grafana for log querying and visualization. It supports multi-tenancy and integrates seamlessly with Prometheus for metrics and alerting.

## Resources Created by the Module

- **resource "helm_release" "loki"**: Installs the Loki Helm chart.
  ```
  resource "helm_release" "loki"
    repository       = "https://grafana.github.io/helm-charts"
    chart            = "loki"
  ```

- **resource "helm_release" "loki_resources"**: Installs additional Loki resources using a dummy Helm chart.

## Variables

The module accepts the following parameters:

- **variable "chart_version"**: The version of the Loki Helm chart to deploy.
  ```hcl
  variable "chart_version" {
    description = "Chart Version"
    type        = string
    default     = "5.41.4"
  }
  ```

- **variable "release_name"**: The name of the Helm release.
  ```hcl
  variable "release_name" {
    description = "Release Name"
    type        = string
    default     = "loki"
  }
  ```

- **variable "namespace"**: The namespace in which to install the Helm release.
  ```hcl
  variable "namespace" {
    description = "Namespace to Install"
    type        = string
    default     = "loki"
  }
  ```

- **variable "hostname"**: The hostname for ingress.
  ```hcl
  variable "hostname" {
    description = "Hostname For Ingress"
    type        = string
    default     = "loki.local"
  }
  ```

- **variable "tenant_name"**: The name for the Loki tenant.
  ```hcl
  variable "tenant_name" {
    description = "Tenant Name"
    type        = string
    default     = "tenant1"
  }
  ```

- **variable "mimir_credentials"**: Credentials for connecting to Mimir.
  ```hcl
  variable "mimir_credentials" {
    description = "Mimir credentials"
    type        = map(string)
    default     = {
      name     = "tenant1"
      password = "secret"
      url      = "http://example.com/api/v1/push"
    }
  }
  ```

- **variable "loki_logs_retention_time"**: The period after which old logs are deleted
```hcl 
variable "loki_logs_retention_time" { 
  default = "360h"
}
```

## Local Values

Local values used for Helm chart configuration:

- **chart_values**: Configuration values for the Loki Helm chart.
  ```hcl
  locals {
    chart_values = <<-VALUES
    ...
    VALUES
  }
  ```

- **loki_resources**: Additional Loki resources configuration.
  ```hcl
  locals {
    loki_resources = <<-VALUES
    ...
    VALUES
  }
  ```

### Example Usage

Here’s an example of how to use the `loki` module in your Terraform script:

```hcl
module "loki" {
  source = "../modules/loki"

  chart_version    = "5.41.4"
  release_name     = "loki"
  namespace        = "loki"
  hostname         = "loki.local"
  tenant_name      = "tenant1"
  mimir_credentials = {
    name     = "tenant1"
    password = "secret"
    url      = "http://example.com/api/v1/push"
  }
}
```

## Functions

Grafana Loki provides a cost-effective and efficient way of aggregating and querying logs. Key functions include:

- **Multi-Tenancy**: Loki supports multiple tenants, allowing logical separation of log data for different teams or environments.
- **Seamless Integration with Grafana**: Loki integrates seamlessly with Grafana for querying and visualizing logs alongside metrics.
- **Efficient Storage**: Loki uses a unique architecture that avoids the need for full-text indexing, making it highly efficient and cost-effective.
- **Scalability**: Loki is designed to be horizontally scalable to handle large amounts of log data.
- **Alerting**: Loki can be integrated with alerting systems like Mimir for comprehensive monitoring and alerting.
- **Versatile Storage Options**: Loki supports various storage backends including local disk and cloud object storage like S3 for flexible and scalable storage solutions.
