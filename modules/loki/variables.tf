variable "chart_version" {
  description = "Chart Version"
  type        = string
  default     = "5.48.0"
}

variable "release_name" {
  description = "Release Name"
  type        = string
  default     = "loki"
}

variable "namespace" {
  description = "Namespace to Install"
  type        = string
  default     = "loki"
}

variable "hostname" {
  description = "Hostname For Ingress"
  type        = string
  default     = "loki.local"
}

variable "tenant_name" {
  description = "Tenant Name"
  type        = string
  default     = "tenant1"
}

variable "loki_logs_retention_time" {
  default    = "360h"
}

variable "loki_minio_storage_size" {
  default    = "32Gi"
}

variable "mimir_credentials" {
  description = "Mimir credentials"
  type        = map(string)
  default = {
    name     = "tenant1"
    password = "secret"
    url      = "http://example.com/api/v1/push"
  }
}

locals {
  chart_values   = <<-VALUES
    gateway:
      replicas: 1
      autoscaling:
        enabled: false
      deploymentStrategy:
        type: Recreate
      ingress:
        enabled: true
        ingressClassName: nginx
        annotations:
          ingress.kubernetes.io/ssl-redirect: "true"
          kubernetes.io/tls-acme: "true"
        hosts:
        - host: ${var.hostname}
          paths:
          - path: "/"
            pathType: Prefix
        tls:
        - secretName: "${replace(var.hostname, ".", "-")}-tls"
          hosts:
          - ${var.hostname}
      basicAuth:
        enabled: true
    loki:
      tenants:
      - name: "${var.tenant_name}"
        password: "${resource.random_password.tenant1.result}"
      - name: "self-monitoring"
        password: "${resource.random_password.self_monitoring.result}"
      commonConfig:
        replication_factor: 1
      storage:
        bucketNames:
          chunks: chunks
          ruler: ruler
          admin: admin
        type: s3
      compactor:
        delete_request_cancel_period: 10m
        retention_enabled: true
        retention_delete_delay: 2h
        delete_request_store: s3      
      limits_config:
        retention_period: ${var.loki_logs_retention_time}               
    deploymentMode: SimpleScalable        
    write:
      replicas: 1
      autoscaling:
        enabled: false
    read:
      replicas: 1
      autoscaling:
        enabled: false
    backend:
      replicas: 1
      autoscaling:
        enabled: false
       
    monitoring:
      dashboards:
        enabled: true
      lokiCanary:
        extraArgs:
        - "-pass=${resource.random_password.self_monitoring.result}"
      selfMonitoring:
        grafanaAgent:
          installOperator: false
      serviceMonitor:
        metricsInstance:
          remoteWrite:
          - url: ${var.mimir_credentials.url}
            basicAuth:
              password:
                key: password
                name: loki-mimir-metrics-crededentials
              username:
                key: username
                name: loki-mimir-metrics-crededentials
            headers:
              X-Scope-OrgID: lokimonitoring
    minio:
      enabled: true
      persistence:
        size: ${var.loki_minio_storage_size}     
    VALUES
  loki_resources = <<-VALUES
    apiVersion: v1
    kind: Secret
    metadata:
      name: loki-mimir-metrics-crededentials
    type: Opaque
    data:
      username: ${base64encode(var.mimir_credentials.name)}
      password: ${base64encode(var.mimir_credentials.password)}
    VALUES
}
