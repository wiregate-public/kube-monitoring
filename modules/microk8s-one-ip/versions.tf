terraform {
  required_version = ">= 1.0"

  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.44.1"
    }

    ansible = {
      source  = "ansible/ansible"
      version = "1.3.0"
    }
  }
}
