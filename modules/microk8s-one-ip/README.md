# microk8s Module

**Important**
This is a universal module for applying the settings of a pre-installed float IP. The setting is applied only if we pass a float IP to this module.


The microk8s module is used for deploying a microk8s cluster on a virtual server hosted by Hetzner.

To operate correctly, this module requires the registration of the following providers:
- `hetznercloud/hcloud`
- `ansible/ansible`

The following resources are created by the module:
- **resource "hcloud_server"**: A virtual server on Hetzner with the specified parameters
- **resource "ansible_playbook" "microk8s"**: An Ansible playbook that installs the necessary software for the microk8s cluster as well as the cluster itself
- **resource "terraform_data"**: Retrieves the "kubeconfig" for the cluster, which is subsequently used for deploying our required services in the cluster

## Variables

The module accepts the following parameters:

- **variable "server_name"**: The name of the virtual server.
  ```hcl
  variable "server_name" {
    description = "Server Name"
    type        = string
    default     = "server1"
  }
  ```

- **variable "server_type"**: The type of server to use.
  ```hcl
  variable "server_type" {
    description = "Server Type"
    type        = string
    default     = "cx11"
  }
  ```

- **variable "server_image"**: The server image to use (operating system).
  ```hcl
  variable "server_image" {
    description = "Server Image"
    type        = string
    default     = "ubuntu-22.04"
  }
  ```

- **variable "datacenter"**: The datacenter where the server will be located.
  ```hcl
  variable "datacenter" {
    description = "Server Datacenter"
    type        = string
    default     = "nbg1-dc3"
  }
  ```

- **variable "ansible_ssh_keys"**: A list of SSH public keys passed to the Ansible provider.
  ```hcl
  variable "ansible_ssh_keys" {
    description = "List of SSH Public Keys passed to Ansible Provider"
    type        = list(string)
    default     = []
  }
  ```

### Example Usage

Here’s an example of how to use the `microk8s` module in your Terraform script:

```hcl
module "microk8s" {
  source           = "../modules/microk8s"
  server_name      = "server1"
  server_type      = "cx11"
  server_image     = "ubuntu-22.04"
  datacenter       = "nbg1-dc3"
  ansible_ssh_keys = ["your-ssh-public-key"]
}
```