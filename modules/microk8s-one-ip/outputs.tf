output "host" {
  value = resource.hcloud_server.server.ipv4_address
}

output "kubeconfig" {
  value     = yamldecode(base64decode(resource.terraform_data.kubeconfig.output))
  sensitive = true
}
