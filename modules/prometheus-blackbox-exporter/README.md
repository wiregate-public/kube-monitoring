# prometheus-blackbox-exporter Module

The `prometheus-blackbox-exporter` module deploys the Prometheus Blackbox Exporter using a Helm chart. The module relies on the Helm chart from the repository [https://prometheus-community.github.io/helm-charts](https://prometheus-community.github.io/helm-charts).

## Overview

The Prometheus Blackbox Exporter allows you to probe endpoints over different protocols like HTTP, HTTPS, DNS, TCP, ICMP, and gRPC. It is useful for monitoring the availability and responsiveness of your endpoints. By defining various probing strategies and conditions, you can gain insights into the performance and health of your services.

## Resources Created by the Module

The module creates the following resource:
- **resource "helm_release" "prometheus_blackbox_exporter"**: This resource installs the Helm chart for the Prometheus Blackbox Exporter.

## Variables

The module accepts the following parameters:

- **variable "chart_version"**: The version of the Helm chart to deploy.
  ```hcl
  variable "chart_version" {
    description = "Chart Version"
    type        = string
    default     = "8.8.0"
  }
  ```

- **variable "release_name"**: The name of the Helm release.
  ```hcl
  variable "release_name" {
    description = "Release Name"
    type        = string
    default     = "prometheus-blackbox-exporter"
  }
  ```

- **variable "namespace"**: The namespace in which to install the Helm release.
  ```hcl
  variable "namespace" {
    description = "Namespace to Install"
    type        = string
    default     = "monitor"
  }
  ```

- **variable "targets"**: A list of targets for the Blackbox Exporter to monitor.
  ```hcl
  variable "targets" {
    description = "Blackbox Targets"
    type        = list(map(string))
    default     = []
  }
  ```

- **variable "additional_modules"**: Additional modules for the Blackbox Exporter.
  ```hcl
  variable "additional_modules" {
    description = "Additional Modules for Blackbox Exporter"
    type        = map(any)
    default     = {}
  }
  ```

## Example Usage

Here’s an example of how to use the `prometheus-blackbox-exporter` module in your Terraform script:

```hcl
module "prometheus_blackbox_exporter" {
  source = "../modules/prometheus-blackbox-exporter"

  chart_version    = "8.8.0"
  release_name     = "prometheus-blackbox-exporter"
  namespace        = "monitor"
  targets          = [
    {
      name = "example-target"
      url  = "http://example.com"
    }
  ]
  additional_modules = {
    http_2xx = {
      prober  = "http"
      timeout = 5 
      http    = {
        valid_http_versions  = ["HTTP/1.1", "HTTP/2"]
        valid_status_codes   = [200, 201]
      }
    }
  }
}
```

## Functions

The Prometheus Blackbox Exporter offers a variety of probing methods to monitor the availability and performance of your endpoints:

- **HTTP/HTTPS Probing**: Monitor web services by sending HTTP/HTTPS requests and matching response status codes, response body contents, and headers.

- **DNS Probing**: Check the availability of DNS records and match returned IP addresses or other records.

- **TCP Probing**: Measure the availability and response time of TCP services by establishing TCP connections to specified ports.

- **ICMP Probing**: Perform ping checks to measure the round-trip time and success rate for ICMP packets, a common method to monitor network latency and packet loss.

- **gRPC Probing**: Monitor gRPC services by sending specific gRPC requests and verifying the responses.

The `blackbox_exporter` allows defining custom probing strategies and conditions to fit diverse monitoring scenarios, making it a powerful tool for endpoint monitoring.
