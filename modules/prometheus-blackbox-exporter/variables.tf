variable "chart_version" {
  description = "Chart Version"
  type        = string
  default     = "8.8.0"
}

variable "release_name" {
  description = "Release Name"
  type        = string
  default     = "prometheus-blackbox-exporter"
}

variable "namespace" {
  description = "Namespace to Install"
  type        = string
  default     = "monitor"
}

variable "targets" {
  description = "Blackbox Targets"
  type        = list(map(string))
  default     = []
}

variable "additional_modules" {
  description = "Additional Modules for Blackbox Exporter"
  type        = map(any)
  default     = {}
}

locals {
  chart_values = {
    serviceMonitor = {
      enabled = true
      selfMonitor = {
        enabled = true
      }
      targets = var.targets
    }
    config = {
      modules = merge(
        {
          http_2xx = {
            http = {
              follow_redirects      = true
              preferred_ip_protocol = "ip4"
              valid_http_versions   = ["HTTP/1.1", "HTTP/2.0"]
            }
            prober  = "http"
            timeout = "5s"
          }
        },
        var.additional_modules
      )
    }
    resources = {
      limits = {
        cpu    = "1"
        memory = "300Mi"
      }
      requests = {
        cpu    = "50m"
        memory = "50Mi"
      }
    }
  }
}
