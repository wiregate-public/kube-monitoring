resource "helm_release" "prometheus_blackbox_exporter" {
  name             = var.release_name
  repository       = "https://prometheus-community.github.io/helm-charts"
  chart            = "prometheus-blackbox-exporter"
  version          = var.chart_version
  namespace        = var.namespace
  create_namespace = true
  values           = [yamlencode(local.chart_values)]
}
