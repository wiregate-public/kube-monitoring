variable "chart_version" {
  description = "Chart Version"
  type        = string
  default     = "4.10.0"
  #default     = "4.9.0"
}

variable "namespace" {
  description = "Namespace to Install"
  type        = string
  default     = "ingress"
}

locals {
  chart_values = <<-VALUES
    controller:
      kind: "DaemonSet"
      service:
        enabled: false
      hostPort:
        enabled: true
        ports:
          http: "80"
          https: "443"
      allowSnippetAnnotations: true
      admissionWebhooks:
        enabled: false
    VALUES
}
