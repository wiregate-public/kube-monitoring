# ingress Module

The `ingress` module installs the `ingress-nginx` controller into a Kubernetes cluster using the Helm chart from the repository [https://kubernetes.github.io/ingress-nginx](https://kubernetes.github.io/ingress-nginx). In this project this module is used for the monitoring server.

## Resources Created by the Module

The module creates the following resource:

- **resource "helm_release" "nginx_ingress"**: Installs the `ingress-nginx` Helm chart.


## Variables

The module accepts the following parameters:

- **variable "chart_version"**: The version of the `ingress-nginx` Helm chart to deploy.
  ```hcl
  variable "chart_version" {
    description = "Chart Version"
    type        = string
    default     = "4.10.0"
  }
  ```

- **variable "namespace"**: The namespace in which to install the Helm release.
  ```hcl
  variable "namespace" {
    description = "Namespace to Install"
    type        = string
    default     = "ingress"
  }
  ```

## Local Values

Local values used for Helm chart configuration:

- **chart_values**: Configuration values for the `ingress-nginx` Helm chart.
  ```hcl
  locals {
    chart_values = <<-VALUES
    ...
    VALUES
  }
  ```

### Example Usage

Here’s an example of how to use the `ingress` module in your Terraform script:

```hcl
module "ingress" {
  source         = "../modules/ingress"
  chart_version  = "4.10.0"
  namespace      = "ingress"
}
```

### Functionality

The `ingress` module provides the following functionalities:

1. **Ingress Controller Installation**: Installs the `ingress-nginx` controller to efficiently manage and route network traffic within the Kubernetes cluster.
2. **Hosting Configuration**: Configured to use `DaemonSet` to ensure that the ingress controller runs on every node, optimizing availability and distribution.
3. **Port Configuration**: Opens both HTTP (port 80) and HTTPS (port 443) ports using the host's port.
4. **Annotation Support**: Allows the use of annotations for fine-grained configuration of ingress resources.
5. **Security & Webhooks**: Admission webhooks are disabled for simplicity and security reasons.

This module is particularly useful for setting up a monitoring server, ensuring that network traffic can be efficiently managed and routed to the monitoring services.
